# -*- coding: utf-8 -*-
"""
    test_get_df
    ~~~~~~~~~~~~~~~


    :copyright: (c) 2020 Shooju
"""
import dateutil.parser
import pandas
from unittest.case import TestCase
from shooju import Connection, sid, Point
from datetime import datetime
import pytz

utc = pytz.utc

from tests.settings import *


def add_dummy_data(conn, series_id, points=None, fields=None):
    job = conn.register_job("unittest.job")

    if fields:
        job.put_fields(series_id, fields)
    if points:
        job.put_points(series_id, points)

    job.submit()
    return job.job_id


class TestGetDataFrame(TestCase):

    def setUp(self):
        self.conn = Connection(server=API_SERVER, user=API_USER, api_key=API_SECRET)
        super(TestGetDataFrame, self).setUp()

    def test_get_df(self,):
        prefix = sid("users", API_USER, "unittest", "get_df")
        series = [
            {
                'series_id': sid(prefix, "1"),
                'points': [Point(1009843200.0, 7370), Point(1012521600.0, 7330), Point(1014940800.0, 7370)]
            },
            {
                'series_id': sid(prefix, "2"),
                'points': [Point(1009843200.0, 7370), Point(1012521600.0, 7330), Point(1014940800.0, 7370)]
            },
            {
                'series_id': sid(prefix, "3"),
                'points': [Point(1009843200.0, 7370), Point(1012521600.0, 7330), Point(1014940800.0, 7370)]
            }
        ]
        for s in series:
            add_dummy_data(self.conn, **s)

        self.conn.raw.post('/series/refresh')

        data_frame = self.conn.get_df('sid:{}'.format(prefix), fields=[], max_points=-1, series_axis='columns')

        # no fields should be a dataframe with series_id, and the dates
        self.assertEqual(sorted(list(data_frame.columns)), [s['series_id'] for s in series])

        for s in series:
            self.assertEqual(len(set(data_frame[s['series_id']])), 2)

        series = [
            {
                'series_id': sid(prefix, "1"),
                'fields': {
                    'country': 'AZERBAIJAN'
                }
            },
            {
                'series_id': sid(prefix, "2"),
                'fields': {
                    'country': 'BAHRAIN'
                }

            },
            {
                'series_id': sid(prefix, "3"),
                'fields': {
                    'country': 'BARBADOS'
                }
            }
        ]
        for s in series:
            add_dummy_data(self.conn, **s)
        self.conn.raw.post('/series/refresh')

        data_frame = self.conn.get_df('sid:{}'.format(prefix), fields=['country'])
        self.assertEqual(sorted(data_frame.columns), ['country', 'series_id'])

        for series_id in data_frame['series_id']:
            self.assertIn(series_id, [s['series_id'] for s in series])

        for country in data_frame['country']:
            self.assertIn(country, [s['fields']['country'] for s in series])

        # fields and points
        series = [
            {
                'series_id': sid(prefix, "1"),
                'points': [Point(1009843200.0, 7370), Point(1012521600.0, 7330), Point(1014940800.0, 7370)],
                'fields': {
                    'country': 'AZERBAIJAN'
                }
            },
            {
                'series_id': sid(prefix, "2"),
                'points': [Point(1009843200.0, 7370), Point(1012521600.0, 7330), Point(1014940800.0, 7370)],
                'fields': {
                    'country': 'BAHRAIN'
                }

            },
            {
                'series_id': sid(prefix, "3"),
                'points': [Point(1009843200.0, 7370), Point(1012521600.0, 7330), Point(1014940800.0, 7370)],
                'fields': {
                    'country': 'BARBADOS'
                }
            }
        ]

        data_frame = self.conn.get_df('sid:{}'.format(prefix), fields=['country'], max_points=-1)

        self.assertEqual(len(data_frame), len(series) * 3)
        self.assertEqual(len(set(data_frame['val'])), 2)

        # column header based on field
        data_frame = self.conn.get_df('sid:{}'.format(prefix), fields=['country'], max_points=-1, series_axis='columns')
        self.assertEqual(sorted(list(data_frame.columns)), ['AZERBAIJAN', 'BAHRAIN', 'BARBADOS'])

        # check formatted fields
        data_frame = self.conn.get_df('sid:{}'.format(prefix), fields=['=Country: {{country}}'], max_points=-1, series_axis='columns')
        self.assertEqual(sorted(list(data_frame.columns)), ['Country: {}'.format(c) for c in ['AZERBAIJAN', 'BAHRAIN', 'BARBADOS']])

        # with operators
        data_frame = self.conn.get_df('sid:{}@A:M'.format(prefix), fields=['country'], max_points=-1)

        self.assertEqual(len(data_frame), len(series))
        self.assertEqual(len(set(data_frame['val'])), 1)

    def test_get_df_with_tz_unaware_series(self):

        series = {
                'series_id': sid("users", API_USER, "unittest", "unaware_tz_series"),
                'points': [Point(dateutil.parser.parse('2017-01-10'), 433), Point(dateutil.parser.parse('2017-01-10'), 765)],
                'fields': {
                    'country': 'OIL EXPORTS'
                }
            }

        add_dummy_data(self.conn, **series)
        self.conn.raw.post('/series/refresh')

        data_frame = self.conn.get_df('sid:users\\{}\\unittest\\unaware_tz_series'.format(API_USER), fields=['country'], max_points=-1, series_axis='columns')
        self.assertEqual(len(data_frame), 1)

    def test_get_df_with_tz_aware_series(self):

        series = {
                'series_id': sid("users", API_USER, "unittest", "aware_tz_series"),
                'points': [Point(dateutil.parser.parse('2017-01-10T00:00+00:00'), 433), Point(dateutil.parser.parse('2017-01-10T00:00-03:00'), 765)],
                'fields': {
                    'country': 'OIL EXPORTS'
                }
            }

        add_dummy_data(self.conn, **series)
        self.conn.raw.post('/series/refresh')

        data_frame = self.conn.get_df('sid:users\\{}\\unittest\\aware_tz_series'.format(API_USER), fields=['country'], max_points=-1, series_axis='columns')
        self.assertEqual(len(data_frame), 2)

        # Adds dummy series tz aware and see if it remains with its tz info after be appended
        dummy_series = pandas.Series([32], index=pandas.DatetimeIndex(['2021-03-15'], tz='America/Chicago'), dtype='float64')
        self.assertEqual(data_frame['OIL EXPORTS'].append(dummy_series).tail(1).index[0].tz.zone, 'America/Chicago')


    def test_get_df_with_tz_mixed_series(self):

        series = {
                'series_id': sid("users", API_USER, "unittest", "mixed_tz_series"),
                'points': [Point(dateutil.parser.parse('2017-01-10'), 433), Point(dateutil.parser.parse('2017-01-10T00:00-03:00'), 765)],
                'fields': {
                    'country': 'OIL EXPORTS'
                }
            }

        add_dummy_data(self.conn, **series)
        self.conn.raw.post('/series/refresh')

        data_frame = self.conn.get_df('sid:users\\{}\\unittest\\mixed_tz_series'.format(API_USER), fields=['country'], max_points=-1, series_axis='columns')
        self.assertEqual(len(data_frame), 2)

        # test with localized series
        data_frame = self.conn.get_df('sid:users\\{}\\unittest\\mixed_tz_series@localize:America/Chicago'.format(API_USER), fields=['country'], max_points=-1, series_axis='columns')
        self.assertEqual(len(data_frame), 2)
        self.assertEqual([row.tz.zone for row in data_frame['OIL EXPORTS'].index], ['America/Chicago', 'America/Chicago'])


    def test_get_df_without_points(self):

        series = {
                'series_id': sid("users", API_USER, "unittest", "no_points"),
                'fields': {
                    'country': 'OIL EXPORTS'
                }
            }

        add_dummy_data(self.conn, **series)
        self.conn.raw.post('/series/refresh')

        data_frame_rows = self.conn.get_df('sid:users\\{}\\unittest\\no_points'.format(API_USER), fields=['*'], max_points=-1)
        self.assertTrue(data_frame_rows.empty)

        data_frame_cols = self.conn.get_df('sid:users\\{}\\unittest\\no_points'.format(API_USER), fields=['*'], max_points=-1, series_axis='columns')
        self.assertTrue(data_frame_cols.empty)

    def test_get_df_points_with_fields(self):
        series = [
            {
                'series_id': sid("users", API_USER, "unittest", "get_df_points_with_fields", "1"),
                'points': [Point(datetime(2020, 1, 1), 1),
                           Point(datetime(2020, 1, 2), 1),
                           Point(datetime(2020, 1, 3), 1)],
                'fields': {
                    'country': 'China',
                    'region': 'Asia'
                }
            },
            {
                'series_id': sid("users", API_USER, "unittest", "get_df_points_with_fields", "2"),
                'points': [Point(datetime(2020, 1, 1), 10),
                           Point(datetime(2020, 1, 2), 11),
                           Point(datetime(2020, 1, 3), 12)],
                'fields': {
                    'country': 'US',
                    'region': 'North America'
                }
            },
            {
                'series_id': sid("users", API_USER, "unittest", "get_df_points_with_fields", "3"),
                'points': [Point(datetime(2020, 1, 1), 21),
                           Point(datetime(2020, 1, 2), 22),
                           Point(datetime(2020, 1, 4), 23)]
            },
            {
                'series_id': sid("users", API_USER, "unittest", "get_df_points_with_fields", "4"),
                'points': [Point(datetime(2020, 1, 1), 31),
                           Point(datetime(2020, 1, 2), 32),
                           Point(datetime(2020, 1, 5), 32)],
                'fields': {
                    'country': 'UK'
                }
            }
        ]
        for s in series:
            add_dummy_data(self.conn, **s)
        self.conn.raw.post('/series/refresh')

        data_frame_rows = self.conn.get_df('sid:users\\{}\\unittest\\get_df_points_with_fields'.format(API_USER), fields=['*'], max_points=-1)
        self.assertEqual(data_frame_rows['dt'].min().to_pydatetime(), utc.localize(datetime(2020, 1, 1)))
        self.assertEqual(data_frame_rows['dt'].max().to_pydatetime(), utc.localize(datetime(2020, 1, 5)))
        self.assertEqual(data_frame_rows['val'].min(), 1)
        self.assertEqual(data_frame_rows['val'].max(), 32)

        self.assertEqual(list(data_frame_rows[data_frame_rows['country'] == 'China']['val']), [1, 1, 1])
        self.assertEqual(list(data_frame_rows[data_frame_rows['country'] == 'US']['val']), [10, 11, 12])
        self.assertEqual(list(data_frame_rows[data_frame_rows['country'] == 'US']['val']), [10, 11, 12])

        self.assertEqual(list(map(str, data_frame_rows['country'])),
                         ['China', 'US', 'nan', 'UK',
                          'China', 'US', 'nan', 'UK',
                          'China', 'US', 'nan', 'UK'])

        self.assertEqual(
            list(map(str, data_frame_rows['dt'])),
            [
                '2020-01-01 00:00:00+00:00', '2020-01-01 00:00:00+00:00', '2020-01-01 00:00:00+00:00', '2020-01-01 00:00:00+00:00',
                '2020-01-02 00:00:00+00:00', '2020-01-02 00:00:00+00:00', '2020-01-02 00:00:00+00:00', '2020-01-02 00:00:00+00:00',
                '2020-01-03 00:00:00+00:00', '2020-01-03 00:00:00+00:00', '2020-01-04 00:00:00+00:00', '2020-01-05 00:00:00+00:00'
            ]
        )

        self.assertEqual(list(map(str, data_frame_rows['region'])),
                         ['Asia', 'North America', 'nan', 'nan',
                          'Asia', 'North America', 'nan', 'nan',
                          'Asia', 'North America', 'nan', 'nan'])

        self.assertEqual(list(data_frame_rows['val']),
                         [1.0, 10.0, 21.0, 31.0,
                          1.0, 11.0, 22.0, 32.0,
                          1.0, 12.0, 23.0, 32.0])
