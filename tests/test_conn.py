from __future__ import absolute_import, division, print_function, unicode_literals
import datetime
import json
import unittest
import calendar
import time
import six
import pytz
import pandas as pd

import shooju
import shooju.hooks.base
from shooju import Connection, ShoojuApiError, ConnectionError, DEFAULT_REQUEST_TIMEOUT, Client, __version__
from shooju import sid, RemoteJob, Point, options, Options
from shooju.points_serializers import milli_tuple, shooju_point, pd_series_localized
import logging
from tests.settings import *

try:
    from unittest import mock
except ImportError:
    import mock


logging.basicConfig(format='%(asctime)s - PID %(process)d - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)


def test_repr():
    t = sid("shooju","resource","one","two")
    assert t == "shooju\\resource\\one\\two"


def date_to_utc(dt):
    return dt + datetime.timedelta(seconds=time.timezone)


class TestPoint(unittest.TestCase):
    """
    Tests the Point object
    """

    def test_to_dict(self):
        d1 = datetime.date(2012, 1, 22)
        d2 = datetime.date(2011, 4, 20)

        p1 = Point(d1, 225.23)
        p2 = Point(d2, 54.3)

        # print("Point 1 : ", p1.to_dict())
        # print("Point 2 : ", p2.to_dict())

    def test_init(self):
      d1 = datetime.date(2012,1,22)
      p1 = Point(d1,20.22)
      assert p1.value == 20.22
      assert p1.date == d1

      u = _convert_to_milli(d1)
      p2 = Point(u,30.11)

      assert p2.value == 30.11
      assert p2.date == d1

      self.assertRaises(ValueError, Point, "rubbish",22)


def _convert_to_milli(dt):
    dd = datetime.datetime(dt.year, dt.month, dt.day, 0, 0, 0, 0)
    return int(calendar.timegm(dd.utctimetuple())) * 1000


def add_dummy_data(conn, series_id, pts, fields={}):

    job = conn.register_job("unittest.job")

    if fields:
        job.put_fields(series_id, fields)

    if pts:
        job.put_points(series_id, pts)

    # now make the submittion part
    assert job.submit() == True
    conn.raw.post('/series/refresh')
    return job.job_id


class TestConnection(unittest.TestCase):
    """
    Testing the connection here
    """

    def setUp(self):
        self.conn = Connection(server=API_SERVER, user=API_USER, api_key=API_SECRET, location='test_client')
        self.conn.raw.delete('/series/cache', params={'q': '*'})

    def tearDown(self):
        self.conn.raw.delete('/series/cache', params={'q': '*'})
        super(TestConnection, self).tearDown()

    def test_authenticate(self):

        with self.assertRaises(ShoojuApiError):
            conn = Connection(server=API_SERVER, user="fakeuser", api_key="fakepassword")
            conn.get_fields('test')

        conn = Connection(API_SERVER, API_USER, API_SECRET)
        conn.get_fields('test')

        with self.assertRaises(ShoojuApiError):
            Connection(server=API_SERVER, email='wrong email', google_oauth_token='wrong token')

        with mock.patch('shooju.Client') as client_cls:
            client_inst = mock.MagicMock()
            client_inst.post.return_value = {
                'user': 'test',
                'api_secret': 'hey'
            }
            client_cls.return_value = client_inst
            Connection(server=API_SERVER, email=GOOGLE_ACCOUNT_EMAIL, google_oauth_token=GOOGLE_REFRESH_TOKEN)
            client_cls.assert_called_with(API_SERVER, u'test', u'hey',
                                          retry_delay=None, location=None, retries=None, hosts=None,
                                          requests_session=None, timeout=None, extra_params={})

    def test_extra_params(self):
        with mock.patch('shooju.requests.post') as requests_mock:

            class _response():
                status_code = 200
                content = b'{"series": [{"series_id": "test"}], "success": true}'
                headers = dict()

            requests_mock.return_value = _response()
            conn = Connection(API_SERVER, API_USER, API_SECRET, extra_params={'key1': 'value1', 'key2': 'value2'})
            conn.get_fields('test')
            params = requests_mock.call_args[1]['params']
            self.assertEqual(params,
                             {'key1': 'value1',
                              'key2': 'value2',
                              'date_format': 'milli',
                              'df': None,
                              'dt': None,
                              'fields': '*',
                              'max_points': 0,
                              'v': __version__,
                              'location_type': 'python',
                              'location': None})

            # overwrite extra_params
            requests_mock.reset_mock()
            requests_mock.return_value = _response()
            conn.shooju_api.get_single_series(series_id='test',
                                              fields='*',
                                              extra_params={'key2': 'new_value2', 'key3': 'new_value3'})
            params = requests_mock.call_args[1]['params']

            self.assertEqual(params, {'date_format': 'milli',
                                      'df': None,
                                      'dt': None,
                                      'fields': '*',
                                      'key1': 'value1',
                                      'key2': 'new_value2',
                                      'key3': 'new_value3',
                                      'location': None,
                                      'location_type': 'python',
                                      'v': __version__})

            # also works when used raw directly
            requests_mock.reset_mock()
            requests_mock.return_value = _response()
            conn.raw.post('/series/refresh', deserializer=json)
            params = requests_mock.call_args[1]['params']
            self.assertEqual(params,
                             {'key1': 'value1',
                              'key2': 'value2',
                              'location': None,
                              'location_type': 'python',
                              'v': __version__}, )

    def test_request_hooks(self):
        class TestBaseHook(shooju.hooks.base.BaseHook):

            def __init__(self) -> None:
                self.caught_response = None

            def before_request(self, request: shooju.hooks.base.Request) -> shooju.hooks.base.Request:
                return shooju.hooks.base.Request(
                    method='post',
                    headers=request.headers,
                    url=request.url,
                    params={'overridden_param': 'y'},
                    data=request.data,
                    data_raw=request.data_raw,
                    data_json={'test': 'test'}
                )

            def after_request(self, response: shooju.hooks.base.Response) -> shooju.hooks.base.Response:
                self.caught_response = response
                return response
        hook = TestBaseHook()

        with mock.patch('shooju.requests.post') as requests_mock:
            class _response():
                status_code = 200
                content = b'{"series": [{"series_id": "test"}], "success": true}'
                headers = dict()

            requests_mock.return_value = _response()
            conn = Connection(
                API_SERVER,
                API_USER,
                API_SECRET,
                hooks=[hook],
            )
            conn.get_fields('test')
            params = requests_mock.call_args[1]['params']
            self.assertEqual(params,
                             {'overridden_param': 'y'})

            self.assertEqual(
                hook.caught_response.json,
                {"series": [{"series_id": "test"}], "success": True},
            )


    def test_anonymous_call(self):
        resp = mock.MagicMock()
        resp.status_code = 200
        session_mock = mock.MagicMock()
        session_mock.get.return_value = resp
        session_mock.post.return_value = resp

        conn = Client(API_SERVER, API_USER, API_SECRET, requests_session=session_mock)
        conn.get('/series', binary_response=True)
        self.assertEqual(session_mock.get.call_args[1]['auth'], (API_USER, API_SECRET))

        session_mock.reset_mock()

        # it will make an anonymous call to  /auth/googleoauth
        conn = Client(API_SERVER, requests_session=session_mock)
        conn.get('/series', binary_response=True)
        self.assertIsNone(session_mock.get.call_args[1]['auth'])

    def test_session(self):
        session_mock = mock.MagicMock()
        post_mock = mock.MagicMock()
        post_mock().status_code = 200
        post_mock().content = options.series_serializer.dumps(
            {"series": [{"series_id": "test\\1"}], "success": True}
        )
        session_mock.post = post_mock

        self.assertEqual(post_mock.call_count, 2)
        sj = Connection(API_SERVER, 'dummy', 'dummy', requests_session=session_mock)
        sj.get_series('sid=test\\1', )
        self.assertEqual(post_mock.call_count, 3)

    def test_timeout(self):
        session_mock = mock.MagicMock()
        post_mock = mock.MagicMock()
        post_mock().status_code = 200
        post_mock().content = options.series_serializer.dumps(
            {"series": [{"series_id": "test\\1"}], "success": True}
        )
        session_mock.post = post_mock

        sj = Connection(API_SERVER, 'dummy', 'dummy', requests_session=session_mock)
        sj.get_series('sid=test\\1', )

        post_kwargs = post_mock.call_args[1]
        self.assertEqual(post_kwargs.get('timeout'), DEFAULT_REQUEST_TIMEOUT)

    def test_get_data(self):
        """
        Getting data from server after submition
        """
        assert self.conn is not None

        d1 = datetime.date(2012,12,1)
        d2 = datetime.date(2012,11,1)
        d3 = datetime.date(2012,10,1)

        p1 = Point(d1,11.12)
        p2 = Point(d2,101.2)
        p3 = Point(d3,10.2)

        series_id = sid("users", API_USER, "unittest", "getdata")
        add_dummy_data(self.conn, series_id, [p1, p2, p3])

        pts = self.conn.get_points(series_id, size=1)
        assert len(pts) == 1
        self.conn.raw.post('/series/refresh')

        ser = self.conn.get_series('sid={}'.format(series_id), max_points=1)
        self.assertEqual(ser['series_id'], '$sid={}'.format(series_id))
        self.assertEqual(len(ser['points']), 1)

        # now get with date range
        pts = self.conn.get_points(series_id, date_start=datetime.date(2012, 1, 1), date_finish=datetime.date(2012, 11, 2))

        # print("POINTS ARE : ", pts)
        assert pts[0].date == d3
        assert pts[1].date == d2

        ser = self.conn.get_series('sid={}'.format(series_id), max_points=10, df=datetime.date(2012, 1, 1), dt=datetime.date(2012, 11, 2))
        assert ser['points'][0].date == d3
        assert ser['points'][1].date == d2

        # lets see on with max and low selection
        pts = self.conn.get_points(series_id, date_start=datetime.date(2012, 11, 1))
        assert pts[0].date == d2
        assert pts[1].date == d1

        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id))

    def test_pandas_localized(self):
        series_id = sid("users", API_USER, "unittest", "pandas_localized", datetime.datetime.utcnow().isoformat())

        fields = {'timezone': 'America/Chicago'}
        points = []
        for d, v in [
            [datetime.date(2011, 1, 1), 10.],
            [datetime.date(2012, 1, 1), 20],
            [datetime.date(2013, 1, 1), 30],
        ]:
            points.append(Point(d, v))

        add_dummy_data(self.conn, series_id, points, fields)

        def _get_mget(s):
            mget = self.conn.mget()
            mget.get_series(s, max_points=-1, serializer=pd_series_localized)
            return mget.fetch()[0]['points']

        ser = self.conn.get_series('sid="%s"' % series_id, max_points=-1, serializer=pd_series_localized)['points']
        # this is utc
        self.assertEqual(
            {k.isoformat(): v for k, v in ser.items()},
            {'2013-01-01T00:00:00+00:00': 30.0,
             '2011-01-01T00:00:00+00:00': 10.0,
             '2012-01-01T00:00:00+00:00': 20.0}
        )

        ser = list(self.conn.scroll('sid="%s"' % series_id, max_points=-1, serializer=pd_series_localized))[0]['points']
        self.assertEqual(
            {k.isoformat(): v for k, v in ser.items()},
            {'2013-01-01T00:00:00+00:00': 30.0,
             '2011-01-01T00:00:00+00:00': 10.0,
             '2012-01-01T00:00:00+00:00': 20.0}
        )

        ser = _get_mget('sid="%s"' % series_id)
        self.assertEqual(
            {k.isoformat(): v for k, v in ser.items()},
            {'2013-01-01T00:00:00+00:00': 30.0,
             '2011-01-01T00:00:00+00:00': 10.0,
             '2012-01-01T00:00:00+00:00': 20.0}
        )
        # this is America/Chicago
        ser = self.conn.get_series(r'sid="%s"@localize' % (series_id,),
                                   max_points=-1,
                                   serializer=pd_series_localized, )['points']

        self.assertEqual(
            {k.isoformat(): v for k, v in ser.items()},
            {'2012-12-31T18:00:00-06:00': 30.0,
             '2010-12-31T18:00:00-06:00': 10.0,
             '2011-12-31T18:00:00-06:00': 20.0}
        )

        ser = list(self.conn.scroll('sid:"%s"@localize' % series_id, max_points=-1, serializer=pd_series_localized))[0]['points']
        self.assertEqual(
            {k.isoformat(): v for k, v in ser.items()},
            {'2012-12-31T18:00:00-06:00': 30.0,
             '2010-12-31T18:00:00-06:00': 10.0,
             '2011-12-31T18:00:00-06:00': 20.0}
        )

        ser = _get_mget(r'sid="%s"@localize' % (series_id,))
        self.assertEqual(
            {k.isoformat(): v for k, v in ser.items()},
            {'2012-12-31T18:00:00-06:00': 30.0,
             '2010-12-31T18:00:00-06:00': 10.0,
             '2011-12-31T18:00:00-06:00': 20.0}
        )

        # this is America/New_York
        ser = self.conn.get_series(r'sid="%s"@localize:America/New_York' % (series_id,),
                                   max_points=-1,
                                   serializer=pd_series_localized, )['points']
        self.assertEqual(
            {k.isoformat(): v for k, v in ser.items()},
            {'2011-12-31T19:00:00-05:00': 20.0,
             '2010-12-31T19:00:00-05:00': 10.0,
             '2012-12-31T19:00:00-05:00': 30.0}
        )

        ser = list(self.conn.scroll('sid:"%s"@localize:America/New_York' % series_id, max_points=-1, serializer=pd_series_localized))[0]['points']
        self.assertEqual(
            {k.isoformat(): v for k, v in ser.items()},
            {'2011-12-31T19:00:00-05:00': 20.0,
             '2010-12-31T19:00:00-05:00': 10.0,
             '2012-12-31T19:00:00-05:00': 30.0}
        )

        ser = _get_mget(r'sid="%s"@localize:America/New_York' % (series_id,))
        self.assertEqual(
            {k.isoformat(): v for k, v in ser.items()},
            {'2011-12-31T19:00:00-05:00': 20.0,
             '2010-12-31T19:00:00-05:00': 10.0,
             '2012-12-31T19:00:00-05:00': 30.0}
        )

        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id))

    def test_snapshots(self):
        series_id = sid("users", API_USER, "unittest", "snapshot_test10", datetime.datetime.utcnow().isoformat())

        # create initial data
        fields = {'color': 'red'}
        points = []
        for d, v in [
            [datetime.date(2011, 1, 1), 10.],
            [datetime.date(2012, 1, 1), 100.01],
            [datetime.date(2013, 1, 1), 3.1],
        ]:
            points.append(Point(d, v))

        snapshot_job_id_a = add_dummy_data(self.conn, series_id, points, fields)
        time.sleep(2)
        snapshot_date_a = datetime.datetime.utcnow()

        # update our series with new data
        fields['color'] = 'green'
        points[0] = Point(datetime.date(2011, 1, 1), 100.)
        points.append(Point(datetime.date(2011, 1, 2), 10))

        snapshot_job_id_b = add_dummy_data(self.conn, series_id, points, fields)
        time.sleep(2)
        snapshot_date_b = datetime.datetime.utcnow()

        # test first job snapshots
        snapshot_job_id_a_sid = '%s@asof:j%s' % (series_id, snapshot_job_id_a)
        pts = self.conn.get_points(snapshot_job_id_a_sid)
        self.assertEqual(len(pts), 3)
        self.assertEqual([p.value for p in pts], [10., 100.01, 3.1])
        self.assertEqual(self.conn.get_series('sid="{}"@asof:j{}'.format(series_id, snapshot_job_id_a), dt=pts[0].datetime, max_points=1)['points'][0].value, 10)
        self.assertEqual(self.conn.get_series('sid="{}"@asof:j{}'.format(series_id, snapshot_job_id_a), fields=['color'])['fields']['color'], 'red')
        self.assertEqual(self.conn.get_fields(snapshot_job_id_a_sid, fields=['*']), {'color': 'red'})

        snapshot_date_a_sid = '%s@asof:%s' % (series_id, snapshot_date_a)
        pts = self.conn.get_points(snapshot_date_a_sid)
        self.assertEqual(len(pts), 3)
        self.assertEqual([p.value for p in pts], [10., 100.01, 3.1])
        self.assertEqual(self.conn.get_series('sid="{}"@asof:{}'.format(series_id, snapshot_date_a), dt=pts[0].datetime, max_points=1)['points'][0].value, 10)
        self.assertEqual(self.conn.get_series('sid="{}"@asof:{}'.format(series_id, snapshot_date_a), fields=['color'])['fields']['color'], 'red')
        self.assertEqual(self.conn.get_fields(snapshot_date_a_sid, fields=['*']), {'color': 'red'})
        self.conn.raw.post('/series/refresh')
        self.assertEqual(self.conn.get_series('sid="{}"@asof:{}'.format(series_id, snapshot_date_a),
                                              fields=['*'])['fields'],
                         {'color': 'red'})

        time.sleep(5)

        # test second job snapshots
        snapshot_job_id_b_sid = '%s@asof:j%s' % (series_id, snapshot_job_id_b)
        pts = self.conn.get_points(snapshot_job_id_b_sid)
        self.assertEqual(len(pts), 4)
        self.assertEqual([p.value for p in pts], [100., 10, 100.01, 3.1])
        self.assertEqual(self.conn.get_series('sid="{}"@asof:j{}'.format(series_id, snapshot_job_id_b), dt=pts[0].datetime, max_points=1)['points'][0].value, 100)
        self.assertEqual(self.conn.get_series('sid="{}"@asof:j{}'.format(series_id, snapshot_job_id_b), fields=['color'])['fields']['color'], 'green')
        self.assertEqual(self.conn.get_fields(snapshot_job_id_b_sid, fields=['*']), {'color': 'green'})

        snapshot_date_b_sid = '%s@asof:%s' % (series_id, snapshot_date_b)
        pts = self.conn.get_points(snapshot_date_b_sid)
        self.assertEqual(len(pts), 4)
        self.assertEqual([p.value for p in pts], [100., 10, 100.01, 3.1])
        self.assertEqual(self.conn.get_series('sid="{}"@asof:{}'.format(series_id, snapshot_date_b), dt=pts[0].datetime, max_points=1)['points'][0].value, 100)
        self.assertEqual(self.conn.get_series('sid="{}"@asof:{}'.format(series_id, snapshot_date_b), fields=['color'])['fields']['color'], 'green')
        self.assertEqual(self.conn.get_fields(snapshot_date_b_sid, fields=['*']), {'color': 'green'})

        mget = self.conn.mget()
        mget.get_points(snapshot_job_id_a_sid)
        mget.get_points(snapshot_date_a_sid)
        mget.get_fields(snapshot_job_id_a_sid, fields=['*'])
        mget.get_fields(snapshot_date_a_sid, fields=['*'])
        pts1, pts2, fields1, fields2 = mget.fetch()
        self.assertEqual(len(pts1), 3)
        self.assertEqual([p.value for p in pts1], [10., 100.01, 3.1])

        self.assertEqual(len(pts2), 3)
        self.assertEqual([p.value for p in pts2], [10., 100.01, 3.1])
        self.assertEqual(fields1, {u'color': u'red'})
        self.assertEqual(fields2, {u'color': u'red'})

        # clean up
        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id))

    def test_get_points_fields_series_does_not_exist(self):
        series_id = sid('does', 'not', 'exist')
        points = self.conn.get_points(series_id)
        self.assertEqual(points, None)

        #point = self.conn.get_point(series_id, datetime.date(2013, 1, 1))
        point = self.conn.get_series('sid:"{}"'.format(series_id), dt=datetime.date(2013, 1, 1), max_points=1)
        self.assertEqual(point, None)

        fields = self.conn.get_fields(series_id, fields='*')
        self.assertEqual(fields, None)

        #field = self.conn.get_field(series_id, 'color')
        field = self.conn.get_series('sid:"{}"'.format(series_id), fields=['color'])
        self.assertEqual(field, None)

    def test_get_point_datetime(self):
        """
        Test get_point with datetime
        """
        d1 = datetime.datetime(2012,12,1)
        d2 = datetime.datetime(2012,11,1)

        p1 = Point(d1,11.12)
        p2 = Point(d2,101.2)

        series_id = sid("users", API_USER, "unittest", "getpointdatetime")
        add_dummy_data(self.conn, series_id, [p1, p2])

        pt = self.conn.get_series('sid:"{}"'.format(series_id), df=d1, max_points=-1)['points'][0]
        assert pt.datetime == d1

        pt = self.conn.get_series('sid:"{}"'.format(series_id), df=d2, max_points=1)['points'][0]
        assert pt.datetime == d2

        # testing date w/ no point
        dt = datetime.datetime(1900, 1, 1)
        pt = self.conn.get_series('sid:"{}"'.format(series_id), dt=dt, max_points=1).get('points')
        self.assertFalse(pt)

        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id))

    def test_get_points_datetime(self):
        """
        Test get_points with datetime parameters
        """


        d1 = datetime.datetime(2012,12,1,10)
        d2 = datetime.datetime(2012,11,1,11)
        d3 = datetime.datetime(2012,10,1,2)

        p1 = Point(d1,11.12)
        p2 = Point(d2,101.2)
        p3 = Point(d3,10.2)

        series_id = sid("users", API_USER, "unittest", "getpointsdatetime")
        add_dummy_data(self.conn, series_id, [p1, p2, p3])

        #now get with date range
        pts = self.conn.get_points(series_id, date_start=datetime.datetime(2012, 1, 1), date_finish=datetime.datetime(2012, 11, 2))

        assert pts[0].datetime == d3
        assert pts[1].datetime == d2

        #lets see on with max and low selection
        pts = self.conn.get_points(series_id, date_start=datetime.datetime(2012, 11, 1))
        assert pts[0].datetime == d2
        assert pts[1].datetime == d1

        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id))

    def test_shooju_options(self):
        # we can't set unknown point serializer
        with self.assertRaises(AssertionError):
            options.point_serializer = 'unknown'

        options.point_serializer = shooju_point  # this is ok

        opts = Options(disable_sjts=True, disable_msgpack=True)
        self.assertEqual((opts.disable_sjts, opts.series_serializer.__name__), (True, 'json'))
        self.assertEqual((opts.disable_msgpack, opts.default_serializer.__name__), (True, 'json'))

        opts = Options(disable_sjts=False, disable_msgpack=True)
        self.assertEqual((opts.disable_sjts, opts.series_serializer.__name__), (False, 'sjts'))
        self.assertEqual((opts.disable_msgpack, opts.default_serializer.__name__), (True, 'json'))

        opts = Options()
        self.assertEqual((opts.disable_sjts, opts.series_serializer.__name__), (False, 'sjts'))
        self.assertEqual((opts.disable_msgpack, opts.default_serializer.__name__), (False, 'msgpack'))

        opts.disable_msgpack = True
        self.assertEqual((opts.disable_sjts, opts.series_serializer.__name__), (False, 'sjts'))
        self.assertEqual((opts.disable_msgpack, opts.default_serializer.__name__), (True, 'json'))

        opts.disable_msgpack = False
        self.assertEqual((opts.disable_sjts, opts.series_serializer.__name__), (False, 'sjts'))
        self.assertEqual((opts.disable_msgpack, opts.default_serializer.__name__), (False, 'msgpack'))

        opts.disable_sjts = True
        self.assertEqual((opts.disable_sjts, opts.series_serializer.__name__), (True, 'msgpack'))
        self.assertEqual((opts.disable_msgpack, opts.default_serializer.__name__), (False, 'msgpack'))

        opts.disable_msgpack = True
        self.assertEqual((opts.disable_sjts, opts.series_serializer.__name__), (True, 'json'))
        self.assertEqual((opts.disable_msgpack, opts.default_serializer.__name__), (True, 'json'))

    def test_get_fields(self):
        """
        Test get_fields
        """
        d1 = datetime.date(2012,12,1)
        p1 = Point(d1,11.12)

        series_id = sid("users", API_USER, "unittest", "getfields")
        fields = {}
        fields["source"] = "bulgaria"
        fields["fuel"] = "gasoline"

        add_dummy_data(self.conn, series_id, [p1], fields)
        self.conn.raw.post('/series/refresh')

        fields = self.conn.get_fields(series_id)

        assert fields['source'] == "bulgaria"
        assert fields['fuel'] == "gasoline"

        ser = self.conn.get_series('sid={}'.format(series_id), fields=['*'])
        self.assertEqual(ser['series_id'], '$sid={}'.format(series_id))
        fields = ser['fields']
        assert fields['source'] == "bulgaria"
        assert fields['fuel'] == "gasoline"

        # now get only those you want
        fields = self.conn.get_fields(series_id, fields=["source"])
        assert fields['source'] == "bulgaria"

        ser = self.conn.get_series('sid={}'.format(series_id), fields=['source'])
        self.assertEqual(ser['series_id'], '$sid={}'.format(series_id))
        fields = ser['fields']
        self.assertEqual(fields, {'source': "bulgaria"})

        field = 'imaginary'
        fields = self.conn.get_fields(series_id, fields=[field])
        self.assertEqual(fields, {})

        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id))
        self.conn.raw.delete('/series/cache', params={'q': '*'})

    def test_register_job(self):
        job_id = self.conn.register_job("test.register.job")
        assert job_id is not None

    def test_delete_data(self):
        """
        Tries to remove data
        TODO : Implement better tests here!
        """
        series_id = sid("users", 'test_delete')

        def create_series():
            job = self.conn.register_job('test')
            job.put_fields(series_id, {'test_field': 'hey'})
            job.submit()

        # single delete with force=False should move to trash
        create_series()
        job = self.conn.register_job('test', batch_size=2)
        job.delete_series('sid="{}"'.format(series_id))
        self.conn.raw.post('/series/refresh')

        # we didn't submit. so this is not yet deleted
        self.assertEqual(self.conn.get_series('sid:{}'.format(series_id), fields=['test_field'])['fields']['test_field'], 'hey')

        job.submit()
        self.conn.raw.post('/series/refresh')

        self.assertEqual(self.conn.get_fields('System\\Trash\\{}'.format(series_id), fields=['test_field'])['test_field'], 'hey')

        # single delete with force=True should finally remove it
        job.delete_series('sid="System\\Trash\\{}"'.format(series_id), force=True)
        job.submit()
        self.conn.raw.post('/series/refresh')

        self.assertFalse(self.conn.get_fields('System\\Trash\\{}'.format(series_id), fields=['test_field']))

        # delete_by_query with force=False should move to trash
        self.conn.raw.delete('/series/cache', params={'q': '*'})
        create_series()
        self.conn.raw.post('/series/refresh')
        job.delete_series('sid:' + series_id, one=False)
        self.conn.raw.post('/series/refresh')

        self.assertEqual(self.conn.get_fields('System\\Trash\\{}'.format(series_id), fields=['test_field'])['test_field'], 'hey')

        # delete_by_query with force=False should move to trash
        self.conn.raw.post('/series/refresh')
        job.delete_series('sid=System\\Trash\\' + series_id, one=False, force=True)
        self.assertFalse(self.conn.get_series('sid="System\\Trash\\{}"'.format(series_id), fields=['test_field']))

    def test_download_file(self):
        """
        Not a strict test !
        """
        try:
            self.conn.download_file("444b4075-d7d5-4827-9e13-6fa05a00065f")
        except Exception:
            pass

    def test_raw_connection(self):
        r = self.conn.raw.get('/status/green')
        self.assertIn('redis', r)
        self.assertIn('cassandra', r)

    def test_search(self):
        with self.conn.register_job('test') as job:
            job.delete_series('sid:*', one=False, force=True)
        d1 = datetime.date(2012,12,1)
        p1 = Point(d1,11.12)

        series_id0 = sid("users", API_USER, "searchterm", 's0')
        series_id1 = sid("users", API_USER, "searchterm", 's1')

        fields1 = {'field1': 'a', 'field2': 'b'}
        fields2 = {'field1': 'b', 'field2': 'a'}

        add_dummy_data(self.conn, series_id0, [p1], fields1)
        add_dummy_data(self.conn, series_id1, [p1], fields2)

        # force update
        self.conn.raw.post('/series/refresh')

        r = list(self.conn.scroll('searchterm', max_points=0))
        self.assertIn(series_id0, [s['series_id'] for s in r])

        for series in self.conn.scroll('searchterm', max_points=0):
            self.assertIn(series['series_id'], (series_id0, series_id1))

        # test scroll sort
        r = list(self.conn.scroll("users", sort="field1 desc", fields='*', max_points=10))
        self.assertEqual(r[0]['series_id'], series_id1)
        self.assertEqual(r[1]['series_id'], series_id0)

        self.assertEqual(r[0]['points'][0].datetime, p1.datetime)
        self.assertEqual(r[0]['points'][0].value, p1.value)

        r = list(self.conn.scroll("users", sort="field1 asc", fields='*', max_points=10))
        self.assertEqual(r[0]['series_id'], series_id0)
        self.assertEqual(r[1]['series_id'], series_id1)

        options.point_serializer = milli_tuple
        r = list(self.conn.scroll("users", sort="field1 desc", fields='*', max_points=10))
        self.assertEqual(r[0]['points'], [(1354320000000, 11.12)])

        r = list(self.conn.scroll("users", sort="field1 desc", fields='*', max_points=10))
        self.assertEqual(r[0]['points'], [(1354320000000, 11.12)])

        options.point_serializer = shooju_point

        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id0))
            job.delete_series('sid="{}"'.format(series_id1))

        # test operators
        with self.conn.register_job('test operators') as job:
            points = [
                Point(d, v) for d, v in [(datetime.datetime(2015, 1, 1), 1),
                                         (datetime.datetime(2015, 1, 2), 2),
                                         (datetime.datetime(2015, 2, 1), 1)]
                ]
            job.put_points('test_operators', points)
            self.conn.raw.post('/series/refresh')
        series = list(self.conn.scroll('job:{}@A:M'.format(job._job_id), max_points=10000))[0]
        self.assertEqual(series['series_id'], 'test_operators@A:M')
        self.assertEqual([p.datetime for p in series['points']], [datetime.datetime(2015, 1, 1), datetime.datetime(2015, 2, 1)])
        self.assertEqual([p.value for p in series['points']], [1.5, 1])

        # test df/dt
        r = list(self.conn.scroll("test_operators", sort="field1 desc",
                                  fields='*', max_points=10,
                                  df=datetime.datetime(2015, 1, 2),
                                  dt=datetime.datetime(2015, 1, 20)))
        self.assertEqual(r[0]['points'][0].datetime, datetime.datetime(2015, 1, 2))
        self.assertEqual(r[0]['points'][0].value,  2.0)


    def test_serializers(self):
        # let's write some test data first
        series_id = sid("users", API_USER, "unittest", "serializers")
        fields = {
            'field2': b'Plutonio 33.2\xc2\xb0API, 0.36%S, TAN=0.15'.decode('utf-8')
        }
        response_fields = {
            u'field2': u'Plutonio 33.2\xb0API, 0.36%S, TAN=0.15'
        }

        # then let's pull using json serializer
        options.disable_sjts = True
        options.disable_msgpack = True

        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id), force=True)
        self.conn.raw.delete('/series/cache', params={'q': '*'})

        add_dummy_data(self.conn, series_id, None, fields)

        # field should be in unicode
        self.assertEqual(self.conn.get_fields(series_id), response_fields)
        self.assertEqual(
            list(self.conn.scroll('sid:{}'.format(series_id), fields=['*']))[0]['fields'],
            response_fields
        )

        # same with msgpack
        options.disable_sjts = True
        options.disable_msgpack = False
        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id), force=True)
        self.conn.raw.delete('/series/cache', params={'q': '*'})

        add_dummy_data(self.conn, series_id, None, fields)
        self.assertEqual(self.conn.get_fields(series_id), response_fields)
        self.assertEqual(
            list(self.conn.scroll('sid:{}'.format(series_id), fields=['*']))[0]['fields'],
            response_fields
        )

        # same with sjts
        options.disable_sjts = False
        options.disable_msgpack = False
        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id), force=True)
        self.conn.raw.delete('/series/cache', params={'q': '*'})

        add_dummy_data(self.conn, series_id, None, fields)

        self.assertEqual(self.conn.get_fields(series_id), response_fields)

        self.assertEqual(
            list(self.conn.scroll('sid:{}'.format(series_id), fields=['*']))[0]['fields'],
            response_fields
        )

    def test_points_serialize(self):
        from shooju.points_serializers import np_array, pd_series
        d1 = datetime.datetime(2012, 12, 1)
        d2 = datetime.datetime(2012, 11, 1)

        p1 = Point(d1, 11.12)
        p2 = Point(d2, 101.2)

        series_id = sid("users", API_USER, "unittest", "points_serializers")
        series_query = f'sid={series_id}'
        add_dummy_data(self.conn, series_id, [p1, p2])

        # by default this is shooju_point
        pts = self.conn.get_points(series_id, max_points=10)
        self.assertTrue(all(isinstance(p, Point) for p in pts))
        pt = self.conn.get_series(series_query, dt=d1, max_points=1)['points'][0]
        self.assertTrue(isinstance(pt, Point))

        # test get_series with shooju_point and various combinations of include_job and include_timestamp
        pt = self.conn.get_series(series_query, max_points=10, extra_params={'include_job': 'n',
                                                                           'include_timestamp': 'n'})['points'][0]
        self.assertEqual((pt.datetime, pt.value), (d2, 101.2))
        pt = self.conn.get_series(series_query, max_points=10, extra_params={'include_job': 'y',
                                                                           'include_timestamp': 'y'})['points'][0]
        job, timestamp = (pt.job, pt.timestamp)
        self.assertEqual(bool(job), bool(timestamp), (True, True))
        pt = self.conn.get_series(series_query, max_points=10, extra_params={'include_job': 'n',
                                                                           'include_timestamp': 'y'})['points'][0]
        self.assertEqual(pt.timestamp, timestamp)
        pt = self.conn.get_series(series_query, max_points=10, extra_params={'include_job': 'y',
                                                                           'include_timestamp': 'n'})['points'][0]
        self.assertEqual(pt.job, job)

        # milli_tuple
        options.point_serializer = milli_tuple
        pts = self.conn.get_points(series_id, max_points=10)
        self.assertEqual(pts, [(1351728000000, 101.2), (1354320000000, 11.12)])
        pt = self.conn.get_series('sid={}'.format(series_id), df=d1, max_points=1)['points'][0]
        self.assertEqual(pt, (1354320000000, 11.12))

        self.assertEqual(self.conn.get_series('sid={}'.format(series_id), max_points=10)['points'],
                         [(1351728000000, 101.2), (1354320000000, 11.12)])

        # test get_series with milli_tuple and various combinations of include_job and include_timestamp
        pt = self.conn.get_series(series_query, max_points=10, extra_params={'include_job': 'n',
                                                                           'include_timestamp': 'n'})['points'][0]
        self.assertEqual(len(pt), 2)
        pt = self.conn.get_series(series_query, max_points=10, extra_params={'include_job': 'y',
                                                                           'include_timestamp': 'y'})['points'][0]
        job, timestamp = pt[2:4]
        self.assertEqual(len(pt), 4)
        pt = self.conn.get_series(series_query, max_points=10, extra_params={'include_job': 'n',
                                                                           'include_timestamp': 'y'})['points'][0]
        self.assertEqual((pt[2]), timestamp)
        pt = self.conn.get_series(series_query, max_points=10, extra_params={'include_job': 'y',
                                                                           'include_timestamp': 'n'})['points'][0]
        self.assertEqual((pt[2]), job)

        # np_array
        options.point_serializer = np_array
        pts = self.conn.get_points(series_id, max_points=10)
        self.assertEqual(pts.tolist(), [(1351728000000, 101.2), (1354320000000, 11.12)])
        pt = self.conn.get_series('sid={}'.format(series_id), df=d1, max_points=1)['points'][0]
        self.assertEqual(pt.tolist(), (1354320000000, 11.12))

        # test get_series with np_array and various combinations of include_job and include_timestamp
        pt = self.conn.get_series(series_query, max_points=10, extra_params={'include_job': 'n',
                                                                           'include_timestamp': 'n'})['points'][0]
        pt = pt.tolist()
        self.assertEqual(len(pt), 2)
        pt = self.conn.get_series(series_query, max_points=10, extra_params={'include_job': 'y',
                                                                           'include_timestamp': 'y'})['points'][0]
        pt = pt.tolist()
        job, timestamp = pt[2:4]
        self.assertEqual(len(pt), 4)
        pt = self.conn.get_series(series_query, max_points=10, extra_params={'include_job': 'n',
                                                                           'include_timestamp': 'y'})['points'][0]
        pt = pt.tolist()
        self.assertEqual((pt[2]), timestamp)
        pt = self.conn.get_series(series_query, max_points=10, extra_params={'include_job': 'y',
                                                                           'include_timestamp': 'n'})['points'][0]
        pt = pt.tolist()
        self.assertEqual((pt[2]), job)

        options.point_serializer = pd_series
        pts = self.conn.get_points(series_id, max_points=10)
        self.assertEqual(pts.values.tolist(), [101.2, 11.12])
        self.assertEqual([d.to_pydatetime() for d in pts.index],
                         [datetime.datetime(2012, 11, 1, 0, 0), datetime.datetime(2012, 12, 1, 0, 0)])
        pt = self.conn.get_series('sid={}'.format(series_id), df=d1, max_points=1)['points'][0]
        self.assertEqual(pt, 11.12)

        options.point_serializer = shooju_point

        # test serializer parameter
        pts = self.conn.get_points(series_id, max_points=10, serializer=shooju_point)
        self.assertTrue(all(isinstance(p, Point) for p in pts))

        pts = self.conn.get_points(series_id, max_points=10, serializer=milli_tuple)
        self.assertEqual(pts, [(1351728000000, 101.2), (1354320000000, 11.12)])

        pts = self.conn.get_points(series_id, max_points=10, serializer=np_array)
        self.assertEqual(pts.tolist(), [(1351728000000, 101.2), (1354320000000, 11.12)])

        pts = self.conn.get_series('sid={}'.format(series_id), max_points=10, serializer=np_array)['points']
        self.assertEqual(pts.tolist(), [(1351728000000, 101.2), (1354320000000, 11.12)])

        pts = self.conn.get_points(series_id, max_points=10, serializer=pd_series)
        self.assertEqual(pts.values.tolist(), [101.2, 11.12])
        self.assertEqual([d.to_pydatetime() for d in pts.index],
                         [datetime.datetime(2012, 11, 1, 0, 0), datetime.datetime(2012, 12, 1, 0, 0)])
        pts = self.conn.get_points(series_id, max_points=0, serializer=pd_series)
        self.assertEqual(len(pts), 0)

        res = list(self.conn.scroll('points_serializers', max_points=10, serializer=np_array))
        self.assertEqual(res[0]['points'].tolist(),
                         [(1351728000000, 101.2), (1354320000000, 11.12)])

        res = list(self.conn.scroll('points_serializers', max_points=10, serializer=np_array))
        self.assertEqual(res[0]['points'].tolist(),
                         [(1351728000000, 101.2), (1354320000000, 11.12)])

    def test_return_series_errors(self):
        d1 = datetime.datetime(2012, 12, 1)
        d2 = datetime.datetime(2012, 11, 1)

        p1 = Point(d1, 11.12)
        p2 = Point(d2, 101.2)

        sid_1 = sid("users", API_USER, "unittest", "return_series_errors_1")
        sid_2 = sid("users", API_USER, "unittest", "return_series_errors_2")
        add_dummy_data(self.conn, sid_1, [p1, p2], fields={'description': 'test series 1'})
        add_dummy_data(self.conn, sid_2, [p1, p2], fields={'description': 'test series 2'})

        self.conn.raw.post('/series/refresh')

        # by default this returns None (not found)
        ser = self.conn.get_series('sid=zzzzz', fields=['*'])
        self.assertIsNone(ser)

        # this one raises an api error
        with self.assertRaises(ShoojuApiError):
            self.conn.get_series('sid={}@badoperator'.format(sid_1), fields=['*'])

        # this also rasies an error in multiget, even though it's partially good request
        mget = self.conn.mget()
        mget.get_series('sid={}'.format(sid_1), fields=['*'])
        mget.get_series('sid={}@badoperator'.format(sid_2), fields=['*'])
        with self.assertRaises(ShoojuApiError):
            mget.fetch()

        with mock.patch('shooju.options.return_series_errors',
                        new_callable=mock.PropertyMock(return_value=True)):
            mget = self.conn.mget()
            mget.get_series('sid={}'.format(sid_1), fields=['*'])
            mget.get_series('sid={}@badoperator'.format(sid_2), fields=['*'])
            resp = mget.fetch()
            self.assertEqual(resp[0]['series_id'], '$sid={}'.format(sid_1))
            self.assertEqual(resp[0]['fields']['description'], 'test series 1')
            self.assertNotIn('error', resp[0])

            self.assertNotIn('fields', resp[1])
            self.assertEqual(resp[1]['error'], 'invalid_parameters')
            self.assertEqual(resp[1]['description'], 'operator BADOPERATOR is not supported')

            # get_series behavies similar
            resp = self.conn.get_series('sid={}@badoperator'.format(sid_2), fields=['*'])
            self.assertNotIn('fields', resp)
            self.assertEqual(resp['error'], 'invalid_parameters')
            self.assertEqual(resp['description'], 'operator BADOPERATOR is not supported')

            # not found is actually error as well
            resp = self.conn.get_series('sid=zzzzz'.format(sid_2), fields=['*'])
            self.assertNotIn('fields', resp)
            self.assertEqual(resp['error'], 'series_not_found')


class TestRemoteJob(unittest.TestCase):
    """
    Tests the RemoteJob class here
    """

    def setUp(self):
        """
        Create here a job
        """
        self.conn = Connection(server=API_SERVER, user=API_USER, api_key=API_SECRET)
        self.series = sid("users", API_USER, "unittest", "series")
        self.job = self.conn.register_job("test.remote.job")

    def test_write_points(self):
        series_id = sid("users", API_USER, "unittest", "test_write_points")

        d1 = datetime.datetime(2012, 11, 1, 0, 0, 0)
        d2 = datetime.datetime(2012, 12, 1, 0, 0, 0)

        self.job = self.conn.register_job("test.write.points", batch_size=3)
        query = "sid={}".format(series_id)

        self.job.write(query, points=[Point(d1, 11.12), Point(d2, 101.2)], remove_others='all')
        self.job.submit()

        pts = self.conn.get_series(query, max_points=-1)['points']

        self.assertEqual([(p.datetime, p.value) for p in pts],
                         [(d1, 11.12),
                          (d2, 101.2)])

        d3 = datetime.datetime(2013, 11, 1, 0, 0, 0)
        d4 = datetime.datetime(2013, 12, 1, 0, 0, 0)

        self.job.write(query, points=[Point(d3, 22.12), Point(d4, 202.2)])
        self.job.submit()

        pts = self.conn.get_series(query, max_points=-1)['points']

        self.assertEqual([(p.datetime, p.value) for p in pts],
                         [(d1, 11.12),
                          (d2, 101.2),
                          (d3, 22.12),
                          (d4, 202.2)])

        d5 = datetime.datetime(2014, 11, 1, 0, 0, 0)
        d6 = datetime.datetime(2014, 12, 1, 0, 0, 0)

        self.job.write(query, points=[Point(d5, 33.12), Point(d6, 303.2)], remove_others='points')
        self.job.submit()

        pts = self.conn.get_series(query, max_points=-1)['points']

        self.assertEqual([(p.datetime, p.value) for p in pts],
                         [(d5, 33.12),
                          (d6, 303.2)])

        self.job.write(query, points=[Point(d1, 11.12), Point(d2, 101.2)], remove_others='all')
        self.job.submit()

        pts = self.conn.get_series(query, max_points=-1)['points']

        self.assertEqual([(p.datetime, p.value) for p in pts],
                         [(d1, 11.12),
                          (d2, 101.2)])

        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id), force=True)

    def test_write_pd_series(self):
        # Test naive date
        series_id = sid("users", API_USER, "unittest", "test_write_pd_points")
        query = "sid={}".format(series_id)
        self.job = self.conn.register_job("test.write.pd.points", batch_size=3)

        d1 = datetime.datetime(2012, 11, 1, 0, 0, 0)
        d2 = datetime.datetime(2012, 12, 1, 0, 0, 0)
        d3 = datetime.datetime(2012, 12, 2, 0, 0, 0)
        d4 = datetime.datetime(2012, 12, 3, 0, 0, 0)

        ser = pd.Series([11.12, 101.2, 103.3, 104.4], index=pd.DatetimeIndex([d1, d2, d3, d4]))

        self.job.write(query, points=ser, remove_others='all')
        self.job.submit()

        pts = self.conn.get_series(query, max_points=-1)['points']

        self.assertEqual([(p.datetime, p.value) for p in pts],
                         [(d1, 11.12),
                          (d2, 101.2),
                          (d3, 103.3),
                          (d4, 104.4)])

        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id))

        self.job = self.conn.register_job("test.write.pd.points", batch_size=3)

        # Test tz-aware date
        d1 = pytz.timezone('Europe/Stockholm').normalize(datetime.datetime(2012, 11, 1, 0, 0, 0, tzinfo=pytz.UTC))
        d2 = pytz.timezone('Europe/Stockholm').normalize(datetime.datetime(2012, 12, 1, 0, 0, 0, tzinfo=pytz.UTC))

        ser = pd.Series([11.12, 101.2], index=pd.DatetimeIndex([d1, d2]))

        self.job.write(query, points=ser, remove_others='all')
        self.job.submit()

        pts = self.conn.get_series(query, max_points=-1)['points']

        self.assertEqual([(p.datetime.replace(tzinfo=pytz.UTC), p.value) for p in pts],
                         [(d1, 11.12),
                          (d2, 101.2)])

        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id))

        ser = pd.Series([11.12, 101.2], index=[_convert_to_milli(d1), _convert_to_milli(d2)])

        with self.assertRaises(AttributeError) as cm:
            self.job.write(query, points=ser, remove_others='all')
            self.job.submit()
        self.assertEqual(cm.exception.args[0],
                         'When supplying pandas series, index must be of type DatetimeIndex')

    def test_write_fields(self):
        series_id = sid("users", API_USER, "unittest", "test_write_fields")

        self.job = self.conn.register_job("test.write.fields", batch_size=3)
        query = "sid={}".format(series_id)

        self.job.write(query, fields={'source': 'china', 'type': 'gas'}, remove_others='all')
        self.job.submit()
        fields = self.conn.get_series(query, fields='*')['fields']
        self.assertEqual(fields, {'source': 'china', 'type': 'gas'})

        self.job.write(query, fields={'size': 'M'})
        self.job.submit()
        fields = self.conn.get_series(query, fields='*')['fields']
        self.assertEqual(fields, {'source': 'china', 'type': 'gas', 'size': 'M'})

        self.job.write(query, fields={'city': 'London'}, remove_others='fields')
        self.job.submit()
        fields = self.conn.get_series(query, fields='*')['fields']
        self.assertEqual(fields, {'city': 'London'})

        self.job.write(query, fields={'country': 'china'}, remove_others='all')
        self.job.submit()
        fields = self.conn.get_series(query, fields='*')['fields']
        self.assertEqual(fields, {'country': 'china'})

        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id), force=True)

    def test_put_point(self):
        """
        Test put_point
        """
        d1 = datetime.date(2012, 12, 1)
        d2 = datetime.date(2012, 11, 1)
        final_dict = {
            'op': 'post',
            'fields': {},
            'points': {
                _convert_to_milli(d1): 11.12,
                _convert_to_milli(d2): 101.2
            }}

        self.job = self.conn.register_job("test.put.point", batch_size=3)
        self.job.put_point(self.series, Point(d1, 11.12))
        self.job.put_point(self.series, Point(d2, 101.2))

        # print self.job._values
        self.assertEqual(self.job._requests[(self.series, None)], final_dict)

        with self.conn.register_job("test.put.point", batch_size=3) as job:
            job.put_point(self.series, Point(d1, 11.12))
            job.put_point(self.series, Point(d2, 101.2))
            assert job._requests[(self.series, None)] == final_dict
        self.assertFalse(job._requests)

    def test_put_points(self):
        """
        Test put_points
        """
        d1 = datetime.date(2012, 12, 1)
        d2 = datetime.date(2012, 11, 1)
        final_dict = {
            'op': 'post',
            'fields': {},
            'points': {
                _convert_to_milli(d1): 11.12,
                _convert_to_milli(d2): 101.2
            }}

        self.job = self.conn.register_job("test.put.points", batch_size=3)
        self.job.put_points(self.series, [Point(d1, 11.12),Point(d2, 101.2)])
        self.assertEqual(self.job._requests[(self.series, None)], final_dict)

    def test_put_field(self):
        final_dict = {
            'points': {},
            'fields': {'source': 'china'},
            'op': 'post'
        }

        self.job = self.conn.register_job("test.put.field", batch_size=2)
        self.job.put_field(self.series, 'source', 'china')

        assert self.job._requests[(self.series, None)] == final_dict

        with self.conn.register_job("test.put.field", batch_size=2) as job:
            job.put_field(self.series, 'source', 'china')
            assert job._requests[(self.series, None)] == final_dict
        self.assertFalse(job._requests)

    def test_put_fields(self):
        final_dict = {
            'points': {},
            'fields': {
                'source': 'china',
                'type': 'gas'
            },
            'op': 'post'
        }

        self.job = self.conn.register_job("test.put.fields", batch_size=3)

        self.job.put_fields(
            self.series,
            {
                'source': 'china',
                'type': 'gas'
            })

        assert self.job._requests[(self.series, None)] == final_dict

    def test_submit(self):
        """
        Tests the submitting part
        """
        self.job.put_field(self.series, 'source', 'china')
        d1 = datetime.date(2012, 12, 1)
        d2 = datetime.date(2012, 11, 1)

        self.job.put_point(self.series, Point(d1, 11.12))
        self.job.put_point(self.series, Point(d2, 101.2))

        self.job.write("sid={}".format(self.series), points=[Point(d1, 12.12)])
        self.job.write("sid={}".format(self.series), points=[Point(d2, 102.2)])

        # now make the submittion part
        assert self.job.submit() == True
        # now we should remove that data we submitted :
        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(self.series))

    def test_reported_date(self):
        series_id = sid("users", API_USER, "unittest", "seriesa")

        self.conn.raw.post('/series/refresh')
        self.conn.raw.delete('/series/cache', params={'q': '*'})
        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id), force=True)
        self.conn.raw.post('/series/refresh')

        reported_date_a = datetime.datetime(2001, 1, 1)
        reported_date_b = datetime.datetime(2010, 1, 1)
        self.job.put_reported_points(series_id,
                                     reported_date=reported_date_a,
                                     points=[Point(datetime.datetime(2005, 1, 1), 5),
                                             Point(datetime.datetime(2006, 1, 1), 6),
                                             Point(datetime.datetime(2007, 1, 1), 7)])

        self.job.put_reported_points(series_id,
                                     reported_date=reported_date_b,
                                     points=[Point(datetime.datetime(2005, 1, 1), 50),
                                             Point(datetime.datetime(2006, 1, 1), 60),
                                             Point(datetime.datetime(2007, 1, 1), 70)])

        self.job.put_points(series_id, [Point(datetime.datetime(2005, 1, 1), -5),
                                        Point(datetime.datetime(2006, 1, 1), -6),
                                        Point(datetime.datetime(2007, 1, 1), -7)])
        self.job.submit()
        self.conn.raw.post('/series/refresh')

        reported_date_a_sid = '%s@repdate:%s' % (series_id, reported_date_a.isoformat()[:10])
        reported_date_b_sid = '%s@repdate:%s' % (series_id, reported_date_b.isoformat()[:10])

        res_a = self.conn.get_points(reported_date_a_sid, max_points=100)
        res_b = self.conn.get_points(reported_date_b_sid, max_points=100)
        pts = self.conn.get_points(series_id, max_points=100)
        self.assertEqual([(p.datetime, p.value) for p in res_a],
                         [(datetime.datetime(2005, 1, 1, 0, 0), 5.0),
                          (datetime.datetime(2006, 1, 1, 0, 0), 6.0),
                          (datetime.datetime(2007, 1, 1, 0, 0), 7.0)])
        self.assertEqual([(p.datetime, p.value) for p in res_b],
                         [(datetime.datetime(2005, 1, 1, 0, 0), 50.0),
                          (datetime.datetime(2006, 1, 1, 0, 0), 60.0),
                          (datetime.datetime(2007, 1, 1, 0, 0), 70.0)])
        self.assertEqual([(p.datetime, p.value) for p in pts],
                         [(datetime.datetime(2005, 1, 1, 0, 0), -5.0),
                          (datetime.datetime(2006, 1, 1, 0, 0), -6.0),
                          (datetime.datetime(2007, 1, 1, 0, 0), -7.0)])

        res_a = self.conn.get_series(u'sid="{}"@repdate:{}'.format(series_id, reported_date_a.isoformat()[:10]),
                                     max_points=100)
        res_b = self.conn.get_series(u'sid="{}"@repdate:{}'.format(series_id, reported_date_b.isoformat()[:10]),
                                     max_points=100)
        self.assertEqual([(p.datetime, p.value) for p in res_a['points']],
                         [(datetime.datetime(2005, 1, 1, 0, 0), 5.0),
                          (datetime.datetime(2006, 1, 1, 0, 0), 6.0),
                          (datetime.datetime(2007, 1, 1, 0, 0), 7.0)])
        self.assertEqual([(p.datetime, p.value) for p in res_b['points']],
                         [(datetime.datetime(2005, 1, 1, 0, 0), 50.0),
                          (datetime.datetime(2006, 1, 1, 0, 0), 60.0),
                          (datetime.datetime(2007, 1, 1, 0, 0), 70.0)])

        reported_dates = self.conn.get_reported_dates(series_id)
        self.assertEqual(reported_dates, [reported_date_a, reported_date_b])

        reported_dates = self.conn.get_reported_dates(job_id=self.job.job_id)
        self.assertEqual(reported_dates, [reported_date_a, reported_date_b])

        reported_dates = self.conn.get_reported_dates(job_id=self.job.job_id, df=datetime.datetime(2002, 1, 1))
        self.assertEqual(reported_dates, [reported_date_b])

        mget = self.conn.mget()
        mget.get_points(reported_date_a_sid, max_points=100)
        mget.get_points(reported_date_b_sid, max_points=100)
        mget.get_points(series_id, max_points=100)
        res_a, res_b, pts = mget.fetch()
        self.assertEqual([(p.datetime, p.value) for p in res_a],
                         [(datetime.datetime(2005, 1, 1, 0, 0), 5.0),
                          (datetime.datetime(2006, 1, 1, 0, 0), 6.0),
                          (datetime.datetime(2007, 1, 1, 0, 0), 7.0)])
        self.assertEqual([(p.datetime, p.value) for p in res_b],
                         [(datetime.datetime(2005, 1, 1, 0, 0), 50.0),
                          (datetime.datetime(2006, 1, 1, 0, 0), 60.0),
                          (datetime.datetime(2007, 1, 1, 0, 0), 70.0)])
        self.assertEqual([(p.datetime, p.value) for p in pts],
                         [(datetime.datetime(2005, 1, 1, 0, 0), -5.0),
                          (datetime.datetime(2006, 1, 1, 0, 0), -6.0),
                          (datetime.datetime(2007, 1, 1, 0, 0), -7.0)])

        self.job.delete_reported(series_id, [reported_date_a])
        reported_dates = self.conn.get_reported_dates(series_id)
        self.assertEqual(reported_dates, [reported_date_b])

        # delete reported dates with both dates list and delete_all flag is not allowed
        with self.assertRaises(ShoojuApiError):
            self.job.delete_reported(series_id, [reported_date_b], delete_all=True)

        # delete all reported dates without delete_all flag is not allowed
        with self.assertRaises(ShoojuApiError):
            self.job.delete_reported(series_id)

        self.job.delete_reported(series_id, delete_all=True)
        reported_dates = self.conn.get_reported_dates(series_id)
        self.assertEqual(reported_dates, [])

        self.job.put_reported_points(series_id,
                                     reported_date=reported_date_a,
                                     points=[Point(datetime.datetime(2005, 1, 1), 5),
                                             Point(datetime.datetime(2006, 1, 1), 6),
                                             Point(datetime.datetime(2007, 1, 1), 7)])

        self.job.put_reported_points(series_id,
                                     reported_date=reported_date_b,
                                     points=[Point(datetime.datetime(2005, 1, 1), 50),
                                             Point(datetime.datetime(2006, 1, 1), 60),
                                             Point(datetime.datetime(2007, 1, 1), 70)])

        reported_dates = self.conn.get_reported_dates(series_id)
        self.assertEqual(reported_dates, [reported_date_a, reported_date_b])

        self.job.delete_reported(series_id, [reported_date_a, reported_date_b])
        reported_dates = self.conn.get_reported_dates(series_id)
        self.assertEqual(reported_dates, [])

        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id), force=True)

    def test_write_reported_points(self):
        series_id = sid("users", API_USER, "unittest", "seriesb")
        series_query = 'sid="{}"'.format(series_id)
        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id), force=True)

        reported_date_a = datetime.datetime(2001, 1, 1)
        reported_date_b = datetime.datetime(2010, 1, 1)
        self.job.write_reported(series_query,
                                reported_date=reported_date_a,
                                points=[Point(datetime.datetime(2005, 1, 1), 5),
                                        Point(datetime.datetime(2006, 1, 1), 6),
                                        Point(datetime.datetime(2007, 1, 1), 7)])

        self.job.write_reported(series_query,
                                reported_date=reported_date_b,
                                points=[Point(datetime.datetime(2005, 1, 1), 50),
                                        Point(datetime.datetime(2006, 1, 1), 60),
                                        Point(datetime.datetime(2007, 1, 1), 70)])

        self.job.write(series_query, points=[Point(datetime.datetime(2005, 1, 1), -5),
                                             Point(datetime.datetime(2006, 1, 1), -6),
                                             Point(datetime.datetime(2007, 1, 1), -7)])
        self.job.submit()
        self.conn.raw.post('/series/refresh')

        reported_date_a_s_query = '%s@repdate:%s' % (series_query, reported_date_a.isoformat()[:10])
        reported_date_b_s_query = '%s@repdate:%s' % (series_query, reported_date_b.isoformat()[:10])

        res_a = self.conn.get_series(reported_date_a_s_query, max_points=100)['points']
        res_b = self.conn.get_series(reported_date_b_s_query, max_points=100)['points']
        pts = self.conn.get_series(series_query, max_points=100)['points']
        self.assertEqual([(p.datetime, p.value) for p in res_a],
                         [(datetime.datetime(2005, 1, 1, 0, 0), 5.0),
                          (datetime.datetime(2006, 1, 1, 0, 0), 6.0),
                          (datetime.datetime(2007, 1, 1, 0, 0), 7.0)])
        self.assertEqual([(p.datetime, p.value) for p in res_b],
                         [(datetime.datetime(2005, 1, 1, 0, 0), 50.0),
                          (datetime.datetime(2006, 1, 1, 0, 0), 60.0),
                          (datetime.datetime(2007, 1, 1, 0, 0), 70.0)])
        self.assertEqual([(p.datetime, p.value) for p in pts],
                         [(datetime.datetime(2005, 1, 1, 0, 0), -5.0),
                          (datetime.datetime(2006, 1, 1, 0, 0), -6.0),
                          (datetime.datetime(2007, 1, 1, 0, 0), -7.0)])

        self.conn.raw.post('/series/refresh')

        reported_dates = self.conn.get_reported_dates(series_query)
        self.assertEqual(reported_dates, [reported_date_a, reported_date_b])

        reported_dates = self.conn.get_reported_dates(job_id=self.job.job_id)
        self.assertEqual(reported_dates, [reported_date_a, reported_date_b])

        reported_dates = self.conn.get_reported_dates(job_id=self.job.job_id, df=datetime.datetime(2002, 1, 1))
        self.assertEqual(reported_dates, [reported_date_b])

        mget = self.conn.mget()
        mget.get_series(reported_date_a_s_query, max_points=100)
        mget.get_series(reported_date_b_s_query, max_points=100)
        mget.get_series(series_query, max_points=100)
        res_a, res_b, pts = mget.fetch()
        self.assertEqual([(p.datetime, p.value) for p in res_a['points']],
                         [(datetime.datetime(2005, 1, 1, 0, 0), 5.0),
                          (datetime.datetime(2006, 1, 1, 0, 0), 6.0),
                          (datetime.datetime(2007, 1, 1, 0, 0), 7.0)])
        self.assertEqual([(p.datetime, p.value) for p in res_b['points']],
                         [(datetime.datetime(2005, 1, 1, 0, 0), 50.0),
                          (datetime.datetime(2006, 1, 1, 0, 0), 60.0),
                          (datetime.datetime(2007, 1, 1, 0, 0), 70.0)])
        self.assertEqual([(p.datetime, p.value) for p in pts['points']],
                         [(datetime.datetime(2005, 1, 1, 0, 0), -5.0),
                          (datetime.datetime(2006, 1, 1, 0, 0), -6.0),
                          (datetime.datetime(2007, 1, 1, 0, 0), -7.0)])
        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id), force=True)

    def test_write_reported_fields(self):
        series_id = sid("users", API_USER, "unittest", "seriesc")
        series_query = 'sid="{}"'.format(series_id)
        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id), force=True)

        reported_date_a = datetime.datetime(2001, 1, 1)
        reported_date_b = datetime.datetime(2010, 1, 1)
        self.job.write_reported(series_query,
                                reported_date=reported_date_a,
                                fields={'country': 'China', 'city': 'Beijing'})

        self.job.write_reported(series_query,
                                reported_date=reported_date_b,
                                fields={'country': 'US', 'city': 'New York'})

        self.job.write(series_query, fields={'country': 'Russia', 'city': 'Moscow'})
        self.job.submit()
        self.conn.raw.post('/series/refresh')

        reported_date_a_s_query = '%s@repfdate:%s' % (series_query, reported_date_a.isoformat()[:10])
        reported_date_b_s_query = '%s@repfdate:%s' % (series_query, reported_date_b.isoformat()[:10])

        res_a = self.conn.get_series(reported_date_a_s_query, fields='*')['fields']
        res_b = self.conn.get_series(reported_date_b_s_query, fields='*')['fields']
        f = self.conn.get_series(series_query, fields='*')['fields']

        self.assertEqual(res_a, {'country': 'China', 'city': 'Beijing'})
        self.assertEqual(res_b, {'country': 'US', 'city': 'New York'})
        self.assertEqual(f, {'country': 'Russia', 'city': 'Moscow'})

        self.conn.raw.post('/series/refresh')

        reported_dates = self.conn.get_reported_dates(series_query, mode='fields')
        self.assertEqual(reported_dates, [reported_date_a, reported_date_b])

        reported_dates = self.conn.get_reported_dates(job_id=self.job.job_id, mode='fields')
        self.assertEqual(reported_dates, [reported_date_a, reported_date_b])

        reported_dates = self.conn.get_reported_dates(job_id=self.job.job_id,
                                                      df=datetime.datetime(2002, 1, 1), mode='fields')
        self.assertEqual(reported_dates, [reported_date_b])

        mget = self.conn.mget()
        mget.get_series(reported_date_a_s_query, fields='*')
        mget.get_series(reported_date_b_s_query, fields='*')
        mget.get_series(series_query, fields='*')
        res_a, res_b, f = mget.fetch()
        self.assertEqual(res_a['fields'], {'country': 'China', 'city': 'Beijing'})
        self.assertEqual(res_b['fields'], {'country': 'US', 'city': 'New York'})
        self.assertEqual(f['fields'], {'country': 'Russia', 'city': 'Moscow'})

        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id), force=True)

    def test_submit_multiple_series(self):
        """
        A more complicated test to see how multiple
        series submition works !
        """
        series_a = sid("users", API_USER, "unittest", "seriesa")
        series_b = sid("users", API_USER, "unittest", "seriesb")

        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_a))
            job.delete_series('sid="{}"'.format(series_b))
        self.conn.raw.post('/series/refresh')
        self.conn.raw.delete('/series/cache', params={'q': '*'})

        self.job.put_field(series_a, 'source', 'china')
        self.job.put_field(series_b, 'source', 'usa')
        d1 = datetime.date(2012, 12, 1)
        d2 = datetime.date(2012, 11, 1)

        p1 = Point(d1, 11.12)
        p2 = Point(d2, 101.2)
        self.job.put_point(series_a, p1)
        self.job.put_point(series_b, p2)

        # now make the submittion part
        assert self.job.submit() == True

        self.conn.raw.post('/series/refresh')
        # now let's try to get them from server to see if they are same
        pts = self.conn.get_points(series_a)
        assert pts[0].date == p1.date
        # assert pts[0].value == p1.value

        pts = self.conn.get_points(series_b)
        assert pts[0].date == p2.date
        # assert pts[0].value == p2.value
        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_a))
            job.delete_series('sid="{}"'.format(series_b))

    def test_deferred_meta(self):
        series_id = sid("users", API_USER, "unittest", "skip_meta_if_no_fields")
        series_query = 'sid={}'.format(series_id)

        with self.conn.register_job('test') as del_job:
            del_job.delete_series('sid="{}"'.format(series_id))

        with self.conn.register_job("test.submit.bulk", batch_size=2) as job:
            job.write(series_query, {'test': 'a'})
            job.write(series_query, points=[Point(datetime.datetime(2020, 1, 1), 10)])

        self.conn.raw.post('/series/refresh')

        job_id = self.conn.raw.post(self.conn.shooju_api.API_JOBS, data_json={
            'notes': 'test',
            'description': 'test',
            'source': 'python'
        })['job_id']
        job = RemoteJob(self.conn, job_id=job_id, batch_size=10, skip_meta_if_no_fields=True)

        job.write(series_query, points=[Point(datetime.datetime(2020, 1, 1), 20)])
        job.submit()

        # this has no effect on meta
        ser = self.conn.get_series(series_query, fields=['meta.points_max'], max_points=-1)
        self.assertEqual(ser['fields']['meta.points_max'], 10)
        self.assertEqual(ser['points'][0].value, 20)
        job.finish()

        with self.conn.register_job('test cleanup') as job:
            job.delete_series(series_query, one=False)

    def test_submit_bulk(self):
        """
        Tests the bulk submition
        """
        #in that test we will add 3 data and will test
        #what have been submitted and what we have left

        seriesb1 = sid("users", API_USER, "unittest", "seriesb1")
        seriesb2 = sid("users", API_USER, "unittest", "seriesb2")
        seriesb3 = sid("users", API_USER, "unittest", "seriesb3")

        with self.conn.register_job('test') as del_job:
            del_job.delete_series('sid="{}"'.format(seriesb1))
            del_job.delete_series('sid="{}"'.format(seriesb2))
            del_job.delete_series('sid="{}"'.format(seriesb3))

        self.conn.raw.post('/series/refresh')

        job = self.conn.register_job("test.submit.bulk", batch_size=2)

        job.put_field(seriesb1, 'source', 'china')
        job.put_field(seriesb2, 'source', 'usa')

        d1 = datetime.date(2012, 12, 1)
        d2 = datetime.date(2012, 11, 1)
        d3 = datetime.date(2012, 10, 1)

        p1 = Point(d1, 11.12)
        p2 = Point(d2, 101.2)
        p3 = Point(d3, 43.2)

        #after those 2 we should submit data remotely
        job.put_point(seriesb1, p1)
        job.put_point(seriesb2, p2)

        self.conn.raw.post('/series/refresh')
        #at that stage we are ready to see if data is there

        #now let's try to get them from server to see if they are same
        pts = self.conn.get_points(seriesb1)

        #print "SENDING : ",p1.to_dict()
        #print "WHAT I GOT : ",pts[0].to_dict()

        assert pts[0].date == p1.date

        pts = self.conn.get_points(seriesb2)
        assert pts[0].date == p2.date

        # it should be empty
        assert self.job._requests == {}

        job.put_point(seriesb3, p3)
        job.put_field(seriesb3, 'source', 'bulgaria')

        # at that stage it should not be there :
        self.assertEqual(self.conn.get_points(seriesb3), None)
        # now we will submit it manually
        assert job.submit() == True

        self.conn.raw.post('/series/refresh')
        pts = self.conn.get_points(seriesb3)
        assert pts[0].date == p3.date

        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(seriesb3))

    def test_bulk_retry(self):
        call = mock.MagicMock()
        call.side_effect = [
            {
                'responses': [
                    {'success': True, 'description': 'series updated'},
                    {'success': False, 'error': 'series_write_error', 'description': 'series is locked for change'}
                ]
            },
            {
                'responses': [
                    {'success': False, 'error': 'series_write_error', 'description': 'series is locked for change'}
                ]
            },
            {
                'responses': [
                    {'success': False, 'error': 'series_write_error',
                     'description': 'series is locked for change third time'}
                ]
            },
            {
                'responses': [
                    {'success': False, 'error': 'series_write_error', 'description': 'series is locked for change'}
                ]
            }
        ]
        conn = Connection(server=API_SERVER, user=API_USER, api_key=API_SECRET)
        try:
            with conn.register_job('test', batch_size=100) as job:
                job._collision_retry_timeout = 0.1
                job._conn.raw._methods['post'] = call
                job.put_fields('series1', {'field': 'hey'})
                job.put_fields('series2', {'field': 'hey2'})
        except Exception as e:
            # got this on 3rd attempt
            self.assertEqual(str(e), 'series_write_error (series is locked for change third time)')
        else:
            raise AssertionError('exception is not raised')

        # what if entire request fails
        conn = Connection(server=API_SERVER, user=API_USER, api_key=API_SECRET)
        call = mock.MagicMock()
        call.side_effect = [
            {'responses': [{'success': True, 'description': 'series updated'}, {'success': False, 'error': 'series_write_error', 'description': 'series is locked for change'}]},
            {'success': False, "error": 'something_went_wrong', 'description': 'server issue'}
        ]
        try:
            with conn.register_job('test', batch_size=100) as job:
                job._collision_retry_timeout = 0.1
                job._conn.raw._call = call

                job.put_fields('series1', {'field': 'hey'})
                job.put_fields('series2', {'field': 'hey2'})
        except ShoojuApiError as e:
            self.assertIn('something_went_wrong', '{}'.format(e))

    def test_delete(self):
        job = self.conn.register_job("test.submit.bulk", batch_size=2)
        # in that test we will add 3 data and will test
        # what have been submitted and what we have left

        seriesb1 = sid("users", API_USER, "unittest", "seriesb1")
        seriesb2 = sid("users", API_USER, "unittest", "seriesb2")

        job.put_field(seriesb1, 'source', 'china')
        job.put_field(seriesb2, 'source', 'usa')
        self.conn.raw.post('/series/refresh')

        # delete series
        job.delete(seriesb1)
        job.submit()
        self.conn.raw.post('/series/refresh')

        # should be in trash now
        fields = self.conn.get_fields('system\\trash\\{}'.format(seriesb1), fields=['meta.job_deleted'])
        self.assertIn(job.job_id, fields['meta.job_deleted'])

        # delete by query
        job.delete_by_query('sid="{}" not tree:system'.format(seriesb2))
        self.conn.raw.post('/series/refresh')
        self.conn.get_fields('system\\trash\\{}'.format(seriesb2), fields=['meta.job_deleted'])
        fields = self.conn.get_fields('system\\trash\\{}'.format(seriesb2), fields=['meta.job_deleted'])
        self.assertIn(job.job_id, fields['meta.job_deleted'])


    def test_delete_series(self):
        job = self.conn.register_job("test.submit.bulk", batch_size=2)
        # in that test we will add 3 data and will test
        # what have been submitted and what we have left

        seriesb1 = sid("users", API_USER, "unittest", "query", "seriesb1")
        seriesb2 = sid("users", API_USER, "unittest", "query", "seriesb2")

        job.put_field(seriesb1, 'source', 'china')
        job.put_field(seriesb2, 'source', 'usa')
        self.conn.raw.post('/series/refresh')

        # delete one series
        job.delete_series(seriesb1, one=True)
        job.submit()
        self.conn.raw.post('/series/refresh')

        # should be in trash now
        fields = self.conn.get_fields('system\\trash\\{}'.format(seriesb1), fields=['meta.job_deleted'])
        self.assertIn(job.job_id, fields['meta.job_deleted'])

        # delete many
        job.delete_series('sid="{}" not tree:system'.format(seriesb2), one=False)
        self.conn.raw.post('/series/refresh')
        self.conn.get_fields('system\\trash\\{}'.format(seriesb2), fields=['meta.job_deleted'])
        fields = self.conn.get_fields('system\\trash\\{}'.format(seriesb2), fields=['meta.job_deleted'])
        self.assertIn(job.job_id, fields['meta.job_deleted'])


class TestFilesUpload(unittest.TestCase):
    def setUp(self):
        """
        Create session
        """
        self.conn = Connection(server=API_SERVER, user=API_USER, api_key=API_SECRET)

    def test_file_upload(self):
        content, name = b'12345', 'test.txt'

        file_id = self.conn.upload_file(six.BytesIO(content), name)

        # check file's metadata
        f_info = self.conn.raw.get('/files/{}'.format(file_id))['file']

        self.assertEqual(f_info['name'], name)
        self.assertEqual(f_info['size'], len(content))

        # check its content
        fp = self.conn.download_file(file_id, )
        self.assertEqual(fp.read(), content)

        # test upload with custom fields
        custom_fields = {'source': 'test'}

        file_id = self.conn.upload_file(six.BytesIO(content), name, custom_fields=custom_fields)
        f_info = self.conn.raw.get('/files/{}'.format(file_id))['file']
        self.assertEqual(f_info['custom'], custom_fields)
        fp = self.conn.download_file(file_id, )
        self.assertEqual(fp.read(), content)

    def test_multipart_upload(self):
        part_content, name = b'12345' * 1024 * 1024, 'test.txt'
        parts_num = 5

        custom_fields = {'source': 'test'}
        mp = self.conn.init_multipart_upload(name, custom_fields=custom_fields)
        for p_num in range(1, parts_num+1):
            mp.upload_part(p_num, part_content)
        file_id = mp.complete()

        f_info = self.conn.raw.get('/files/{}'.format(file_id))['file']

        self.assertEqual(f_info['name'], name)
        self.assertEqual(f_info['size'], len(part_content) * parts_num)
        self.assertEqual(f_info['custom'], custom_fields)

        # check its content
        fp = self.conn.download_file(file_id, )
        self.assertEqual(fp.read(), part_content * parts_num)


class TestUploaderSession(unittest.TestCase):

    def setUp(self):
        """
        Create session
        """
        self.conn = Connection(server=API_SERVER, user=API_USER, api_key=API_SECRET)
        self.session = self.conn.create_uploader_session()

    def test_session(self):
        # upload file
        content, filename = 'hey',  'test.file'
        file_id = self.session.upload_file(six.StringIO(content), filename)

        # we should see that file in list of files
        time.sleep(5.)
        res = self.conn.raw.get('/files', params={
            'per_page': 1000
        })['results']
        self.assertIn(file_id, [i['file_id'] for i in res])
        # and name
        self.assertIn(filename, [i['name'] for i in res])

        # let's download file
        self.assertEqual(
            self.conn.raw.get('/files/{}/download'.format(file_id), binary_response=True).content.decode('utf-8'),
            content
        )

    def test_multipart_upload(self):
        part_content, name = b'12345' * 1024 * 1024, 'test.txt'
        parts_num = 5

        file_id = self.session.init_multipart(name)
        for p_num in range(1, parts_num + 1):
            self.session.upload_part(file_id, p_num, part_content)
        self.session.complete_multipart(file_id)

        f_info = self.conn.raw.get('/files/{}'.format(file_id))['file']

        self.assertEqual(f_info['name'], name)
        self.assertEqual(f_info['size'], len(part_content) * parts_num)

        # check its content
        fp = self.conn.download_file(file_id, )
        self.assertEqual(fp.read(), part_content * parts_num)


class TestGetBulk(unittest.TestCase):
    """
    Test GetBulk
    """
    def setUp(self):
        """
        Create here a job
        """
        self.conn = Connection(server=API_SERVER, user=API_USER, api_key=API_SECRET)
        self.conn.raw.delete('/series/cache', params={'q': '*'})
        self.get_bulk = self.conn.mget()

    def test_get_points(self):
        """
        Test get_points
        """
        d1 = datetime.date(2012,12,1)
        d2 = datetime.date(2012,11,1)
        d3 = datetime.date(2012,10,1)

        p1 = Point(d1,11.12)
        p2 = Point(d2,101.2)
        p3 = Point(d3,10.2)

        series_id = sid("users", API_USER, "unittest", "getpoints")
        other_sid = sid('does', 'not', 'exist')
        add_dummy_data(self.conn, series_id, [p1, p2, p3])

        self.get_bulk.get_points(series_id, size=1)

        result = self.get_bulk.fetch()
        assert len(result[0]) == 1
        assert result[0][0].date == d3

        options.point_serializer = milli_tuple
        # milli_tuple serializer
        self.get_bulk.get_points(series_id, size=1)
        self.get_bulk.get_points(series_id, date_start=d2, size=1)
        self.get_bulk.get_points(series_id, date_finish=datetime.datetime(1900, 1, 1), size=1)
        result = self.get_bulk.fetch()

        self.assertEqual(len(result[0]), 1)
        self.assertEqual(datetime.datetime.utcfromtimestamp(result[0][0][0] / 1000).date(), d3)

        self.assertEqual(result[1][0], (1351728000000, 101.2))
        self.assertFalse(result[2])

        r = self.conn.get_points(series_id, size=1)
        self.assertEqual(r, [(1349049600000, 10.2)])
        r = self.conn.get_series('sid="{}"'.format(series_id), df=d2, max_points=1)['points'][0]

        self.assertEqual(r, (1351728000000, 101.2))

        r = self.conn.get_series('sid="{}"'.format(series_id), dt=datetime.datetime(1900, 1, 1), max_points=1).get('points')
        self.assertFalse(r)

        options.point_serializer = shooju_point

        #now make 2 get requests to test it

        #that one will pull only d1
        self.get_bulk.get_points(series_id, date_start=datetime.date(2012, 11, 2))

        # that one will get d2 and d3
        self.get_bulk.get_points(series_id, date_finish=datetime.date(2012, 11, 2))

        # get points from non existant series
        self.get_bulk.get_points(other_sid)

        result = self.get_bulk.fetch()
        # print("RESULT 0 : ",result[0])
        assert len(result[0]) == 1
        assert result[0][0].date == d1

        # test the second bulk here
        assert len(result[1]) == 2
        assert result[1][0].date == d3
        assert result[1][1].date == d2

        self.assertEqual(result[2], None)
        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id))

    def test_series_queries(self):
        d1 = datetime.date(2012, 12, 1)
        d2 = datetime.date(2012, 11, 1)
        d3 = datetime.date(2012, 10, 1)

        p1 = Point(d1, 11.12)
        p2 = Point(d2, 101.2)
        p3 = Point(d3, 10.2)

        series_id = sid("users", API_USER, "unittest", "mget_test")
        other_sid = sid('does', 'not', 'exist')
        add_dummy_data(self.conn, series_id, [p1, p2, p3], {'field1': 'field', 'field2': 'field too'})
        self.get_bulk.get_series('sid={}'.format(series_id), max_points=10, fields='*')

        resp = self.get_bulk.fetch()
        self.assertEqual(len(resp), 1)
        self.assertEqual(resp[0]['series_id'], '$sid={}'.format(series_id))
        self.assertEqual(len(resp[0]['points']), 3)
        self.assertEqual(resp[0]['fields'], {'field1': 'field', 'field2': 'field too'})

        self.get_bulk.get_series('sid={}'.format(series_id), max_points=10, fields='*')
        self.get_bulk.get_series('sid={}'.format(series_id), max_points=1, fields=['field1'])
        self.get_bulk.get_series('sid={}'.format(other_sid), max_points=1, fields=['field1'])

        resp = self.get_bulk.fetch()
        self.assertEqual(len(resp), 3)
        self.assertEqual(resp[0]['series_id'], '$sid={}'.format(series_id))
        self.assertEqual(len(resp[0]['points']), 3)
        self.assertEqual(resp[0]['fields'], {'field1': 'field', 'field2': 'field too'})

        self.assertEqual(resp[1]['series_id'], '$sid={}'.format(series_id))
        self.assertEqual(len(resp[1]['points']), 1)
        self.assertEqual(resp[1]['fields'], {'field1': 'field'})

        self.assertEqual(resp[2], None)

    def test_relative_delta(self):
        series_id = sid("users", API_USER, "unittest", "relative_series_test")
        now = datetime.datetime.utcnow()
        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id))

        def _rel_delt(days):
            return datetime.datetime(*(now - datetime.timedelta(days=days)).timetuple()[:3])

        points = sorted([
            (_rel_delt(10), 1),
            (_rel_delt(30), 1),
            (_rel_delt(60), 1),
            (_rel_delt(90), 1),
            (_rel_delt(365), 1),
            (_rel_delt(365 * 2), 1),
            (_rel_delt(365 * 3), 1),
            (_rel_delt(365 * 4), 1),
        ])

        with self.conn.register_job('test') as job:
            job.put_points(series_id, [Point(*p) for p in points])

        self.conn.raw.post('/series/refresh')

        def _datetime_tuple(pts, *args, **kwargs):
            return [(datetime.datetime.utcfromtimestamp(p[0] / 1000), p[1]) for p in pts]

        res = self.conn.get_points(series_id, max_points=-1,
                                   date_start='-2y -1d', date_finish='-1y +1d', serializer=_datetime_tuple)
        self.assertEqual(points[2:4], res)

        res = self.conn.get_series('sid="{}"'.format(series_id), max_points=-1,
                                   df='-2y -1d', dt='-1y +1d', serializer=_datetime_tuple)
        self.assertEqual(points[2:4], res['points'])

        mget = self.conn.mget()
        mget.get_points(series_id, max_points=-1,
                        date_start='-2y -1d', date_finish='-1y +1d', serializer=_datetime_tuple)
        res = mget.fetch()[0]
        self.assertEqual(points[2:4], res)

        res = list(self.conn.scroll('sid:{}'.format(series_id), max_points=1000,
                               df='-2y -1d', dt='-1y +1d', serializer=_datetime_tuple, ))[0]['points']

        self.assertEqual(points[2:4], res)

        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id))

    def test_points_serialize(self):
        from shooju.points_serializers import np_array, pd_series
        d1 = datetime.datetime(2012, 12, 1)
        d2 = datetime.datetime(2012, 11, 1)

        p1 = Point(d1, 11.12)
        p2 = Point(d2, 101.2)

        series_id = sid("users", API_USER, "unittest", "points_serializers")
        add_dummy_data(self.conn, series_id, [p1, p2])

        mget = self.conn.mget()

        # test serializer parameter
        mget.get_points(series_id, max_points=10, serializer=shooju_point)
        mget.get_points(series_id, max_points=10, serializer=milli_tuple)
        mget.get_points(series_id, max_points=10, serializer=np_array)
        mget.get_points(series_id, max_points=10, serializer=pd_series)

        pts1, pts2, pts3, pts4 = mget.fetch()

        self.assertTrue(all(isinstance(p, Point) for p in pts1))
        self.assertEqual(pts2, [(1351728000000, 101.2), (1354320000000, 11.12)])
        self.assertEqual(pts3.tolist(), [(1351728000000.0, 101.2), (1354320000000.0, 11.12)])
        self.assertEqual(pts4.values.tolist(), [101.2, 11.12])
        self.assertEqual([d.to_pydatetime() for d in pts4.index],
                         [datetime.datetime(2012, 11, 1, 0, 0), datetime.datetime(2012, 12, 1, 0, 0)])

    def test_get_fields(self):
        """
        Test get fields for bulk data
        """
        d1 = datetime.date(2012,12,1)
        d2 = datetime.date(2012,11,1)
        d3 = datetime.date(2012,10,1)

        p1 = Point(d1,11.12)
        p2 = Point(d2,101.2)
        p3 = Point(d3,10.2)

        series_id = sid("users", API_USER, "unittest", "getfields1")
        series_id2 = sid("users", API_USER, "unittest", "getfields2")
        other_sid = sid('does', 'not', 'exist')

        add_dummy_data(self.conn, series_id, [p1, p2], fields={'method':'get_points'})
        add_dummy_data(self.conn, series_id2, [p3], fields={'method':'get_fields', 'other':'field'})

        self.get_bulk.get_fields(series_id, fields=['method'])
        self.get_bulk.get_fields(series_id2, fields=['other'])
        self.get_bulk.get_fields(other_sid)

        result = self.get_bulk.fetch()

        assert result[0]['method'] == "get_points"
        assert result[1]['other'] == "field"
        self.assertRaises(AttributeError, getattr, result[1], "method")

        self.assertEqual(result[2], None)

        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id))
            job.delete_series('sid="{}"'.format(series_id2))

    def test_write_points_with_milli(self):
        # highest granularity is up to 1000 microseconds
        dt = datetime.datetime(2001, 1, 1, microsecond=888000)
        pt = Point(dt, 1000)
        series_id = sid("users", API_USER, "unittest", "write_milli")
        add_dummy_data(self.conn, series_id, [pt], fields={})

        ser = self.conn.get_series(r'sid="{}"'.format(series_id), max_points=-1)

        self.assertEqual(ser['points'][0].value, pt.value)
        self.assertEqual(ser['points'][0].datetime, pt.datetime)

        # try delete it
        add_dummy_data(self.conn, series_id, [Point(dt, None)], fields={})

        ser = self.conn.get_series(r'sid="{}"'.format(series_id), max_points=-1)
        self.assertNotIn('points', ser)

        with self.conn.register_job('test') as job:
            job.delete_series('sid="{}"'.format(series_id))


    def test_custom_endpoints(self):
        conn = Connection(server=API_SERVER, user="fakeuser",
                          api_key="fakepassword",
                          hosts=['http://localhost:9000', 'http://localhost:9001'],
                          retries=5, retry_delay=0.1)

        resp = mock.MagicMock()
        resp.status_code = 502

        with mock.patch('shooju.requests.post') as req_mock:
            req_mock.return_value = resp
            with self.assertRaises(ConnectionError):
                conn.get_series(r'sid=test\2', max_points=-1, )

            calls =req_mock.call_args_list
            self.assertEqual(
                [c[0][0] for c in calls],
                [u'http://localhost:9000/api/1/series',
                 u'http://localhost:9001/api/1/series',
                 u'http://localhost:9000/api/1/series',
                 u'http://localhost:9001/api/1/series',
                 u'http://localhost:9000/api/1/series'])

            self.assertEqual(
                [c[1]['headers']['Host'] for c in calls],
                [six.moves.urllib.parse.urlparse(API_SERVER).netloc] * 5
            )
