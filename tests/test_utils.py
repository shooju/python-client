from datetime import datetime
import pytz
import calendar

from unittest.case import TestCase

from shooju import to_milli
from shooju.utils.convert import milli_tuple_naive_to_pandas_tz_aware

try:
    from unittest import mock
except ImportError:
    import mock

chicago = pytz.timezone('America/Chicago')


def datetime_array_to_milli_array(pts):
    for i, p in enumerate(pts):
        pts[i] = to_milli(p[0]), p[1]
    return pts


class TestConverters(TestCase):
    def test_milli_tuple_to_pd_series_localized(self):
        pts = datetime_array_to_milli_array([
            (datetime(2018, 11, 3, 23), -100),
            (datetime(2018, 11, 4, 0), 0),
            (datetime(2018, 11, 4, 1), 100),
            (datetime(2018, 11, 4, 1), 200),
            (datetime(2018, 11, 4, 2), 300),
            (datetime(2018, 11, 4, 3), 400),
            (datetime(2018, 11, 4, 4), 500),
            (datetime(2018, 11, 4, 5), 600),
            (datetime(2018, 11, 4, 6), 700),
            (datetime(2018, 11, 4, 7), 800),
        ])

        # test on dst transition
        ser = milli_tuple_naive_to_pandas_tz_aware(pts, tz="America/Chicago")
        self.assertEqual(
            {k.isoformat(): v for k, v in ser.items()},
            {
                '2018-11-03T23:00:00-05:00': -100,
                '2018-11-04T00:00:00-05:00': 0,
                '2018-11-04T01:00:00-05:00': 100,
                '2018-11-04T01:00:00-06:00': 200,
                '2018-11-04T02:00:00-06:00': 300,
                '2018-11-04T03:00:00-06:00': 400,
                '2018-11-04T04:00:00-06:00': 500,
                '2018-11-04T05:00:00-06:00': 600,
                '2018-11-04T06:00:00-06:00': 700,
                '2018-11-04T07:00:00-06:00': 800,
            }
        )


    def test_datetime_to_milli(self):
        def _to_milli(d):
            return to_milli(d)

        for dt in [
            datetime(2001, 1, 1, 1, 1),
            datetime(2018, 11, 5, 1, 1),
            datetime(2019, 6, 1, 2, 3, 4),
            datetime(2019, 12, 31, 1, 1, 6),
        ]:
            self.assertEqual(_to_milli(dt), self._timegm_milli(dt))
            dt = chicago.localize(dt)
            self.assertEqual(_to_milli(dt), self._timegm_milli(dt))


    @staticmethod
    def _timegm_milli(d):
        return int(calendar.timegm(d.utctimetuple())) * 1000