# -*- coding: utf-8 -*-
"""
    test_remote_job
    ~~~~~~~~~~~~~~~


"""
from datetime import datetime
import sys

from unittest.case import TestCase
try:
    from unittest import mock
except ImportError:
    import mock


from shooju import RemoteJob, ShoojuApiError, Point, sid, Connection

from tests.settings import *

if sys.version_info[0] >= 3:
    xrange = range


class TestRemoteJob(TestCase):

    def test_pre_submit_hooks(self):
        conn = mock.MagicMock()
        hook1 = mock.MagicMock()
        hook2 = mock.MagicMock()

        pre_hooks = [hook1, hook2]
        job_id = 1
        job = RemoteJob(conn, job_id, 3, pre_hooks=pre_hooks)

        self.assertEqual(job._pre_hooks, pre_hooks)

        # adding 3 series to go over the batch size
        for i in xrange(3):
            job.put_field('s%d' % i, 'this', 'that')

        hook1.assert_called_once_with(job_id=job_id)
        hook2.assert_called_once_with(job_id=job_id)

        # should call the hooks again
        job.submit()

        for hook in pre_hooks:
            self.assertEqual(hook.call_count, 2)

    def test_post_submit_hooks(self):
        conn = mock.MagicMock()
        hook1 = mock.MagicMock()
        hook2 = mock.MagicMock()

        response = 'test'
        conn.shooju_api.series_write.return_value = response

        post_hooks = [hook1, hook2]
        job_id = 1
        job = RemoteJob(conn, job_id, 3, post_hooks=post_hooks)

        self.assertEqual(job._post_hooks, post_hooks)
        self.assertEqual(job._pre_hooks, [])

        # adding 3 series to go over the batch size
        for i in xrange(3):
            job.put_field('s%d' % i, 'this', 'that')

        hook1.assert_called_once_with(job_id=job_id, response={u'responses': response, u'success': True})
        hook2.assert_called_once_with(job_id=job_id, response={u'responses': response, u'success': True})

        # should call the hooks again
        job.submit()

        for hook in post_hooks:
            self.assertEqual(hook.call_count, 2)

    def test_add_hooks(self):
        conn = mock.MagicMock()

        job = RemoteJob(conn, 1, 1)

        def test_fn(**kwargs):
            pass

        not_a_fn = 1

        job.add_post_submit_hook(test_fn)
        self.assertEqual(job._post_hooks, [test_fn])
        self.assertEqual(job._pre_hooks, [])

        with self.assertRaises(ValueError):
            job.add_post_submit_hook(not_a_fn)

        job.add_pre_submit_hook(test_fn)
        self.assertEqual(job._pre_hooks, [test_fn])

        with self.assertRaises(ValueError):
            job.add_pre_submit_hook(not_a_fn)

    def test_remote_job_flush_error_after_api_error(self):
        conn = mock.MagicMock()
        conn.shooju_api.series_write.side_effect = ShoojuApiError()

        job = RemoteJob(conn, 1, 5)

        job.put_field('s1', 'color', 'red')
        self.assertEqual(job._cur_batch_size, 1)
        with self.assertRaises(ShoojuApiError):
            job.submit()

        self.assertEqual(job._cur_batch_size, 0)


        # now raise another type of error
        conn.shooju_api.series_write.side_effect = ValueError

        job.put_field('s1', 'color', 'red')
        self.assertEqual(job._cur_batch_size, 1)

        with self.assertRaises(ValueError):
            job.submit()

        self.assertEqual(job._cur_batch_size, 0)

    def test_remove_points(self):

        conn = mock.MagicMock()

        job_id = 1
        job = RemoteJob(conn, job_id, 3)

        series_id = sid('test', 'series')
        dt = datetime(2014, 1, 1)
        point = Point(dt, 1)

        job.remove_points(series_id)
        self.assertIn(series_id, job._remove)
        self.assertEqual(job._remove[series_id], {'points': True, 'fields': False})

        job.put_point(series_id, point)

        job.submit()

        self.assertFalse(job._remove)  # remove should be empty

        self.assertEqual(conn.shooju_api.series_write.call_count, 1)
        args, kwargs = conn.shooju_api.series_write.call_args

        self.assertEqual(kwargs['job_id'], job_id)

        self.assertIn('keep_only', args[0][0])
        self.assertEqual(args[0][0]['keep_only'], 'points')

    def test_remove_fields(self):

        conn = mock.MagicMock()

        job_id = 1
        job = RemoteJob(conn, job_id, 3)

        series_id = sid('test', 'series')

        fields = {'color': 'blue'}

        job.remove_fields(series_id)
        self.assertIn(series_id, job._remove)
        self.assertEqual(job._remove[series_id], {'points': False, 'fields': True})

        job.put_fields(series_id, fields)
        job.submit()

        self.assertFalse(job._remove)  # remove should be empty

        self.assertEqual(conn.shooju_api.series_write.call_count, 1)
        args, kwargs = conn.shooju_api.series_write.call_args

        self.assertEqual(kwargs['job_id'], job_id)

        self.assertIn('keep_only', args[0][0])
        self.assertEqual(args[0][0]['keep_only'], 'fields')

    def test_remove_fields_and_points(self):

        conn = mock.MagicMock()

        job_id = 1
        job = RemoteJob(conn, job_id, 3)

        series_id = sid('test', 'series')

        fields = {'color': 'blue'}

        job.remove_points(series_id)
        job.remove_fields(series_id)
        self.assertIn(series_id, job._remove)
        self.assertEqual(job._remove[series_id], {'points': True, 'fields': True})

        job.put_fields(series_id, fields)
        job.submit()

        self.assertFalse(job._remove)  # remove should be empty

        self.assertEqual(conn.shooju_api.series_write.call_count, 1)
        args, kwargs = conn.shooju_api.series_write.call_args

        self.assertEqual(kwargs['job_id'], job_id)

        self.assertIn('keep_only', args[0][0])
        self.assertEqual(args[0][0]['keep_only'], 'all')

    def test_remove_methods_reset_after_submit(self):
        conn = mock.MagicMock()
        job_id = 1
        job = RemoteJob(conn, job_id, 3, async_mode=True)

        job.remove_points('s1')
        job.remove_points('s2')
        job.remove_fields('s3')

        job.submit()
        # empty submit does nothing. b/c we didn't write anything
        self.assertEqual(len(job._remove), 3)

    def test_requests_order(self):
        conn = mock.MagicMock()
        job_id = 1
        job = RemoteJob(conn=conn, job_id=job_id, batch_size=50)
        series_ids = ['series_id_{}'.format(i) for i in range(10)]

        fields = {'field1': 'test1', 'field2': 'test2'}
        points = [Point(datetime(2015, 1, d), d) for d in xrange(1, 10)]

        for series_id in series_ids:
            job.put_fields(series_id, fields)
            job.put_points(series_id, points)

        job.submit()

        # let's make sure series were sent in same order
        bulk_body = conn.method_calls[0][1][0]
        self.assertEqual([s['series_id'] for s in bulk_body], series_ids)

    def test_remove_fields_against_api(self):
        series_ids = [sid('test', 'series', str(s)) for s in range(10)]

        conn = Connection(server=API_SERVER, user=API_USER, api_key=API_SECRET)

        # create series
        fields = {'field_1': 'value_1', 'field_2': 'value_2'}
        with conn.register_job('test', batch_size=5) as job:
            for s in series_ids:
                job.put_fields(s, fields)

        # check series fields
        for s in series_ids:
            self.assertEqual(conn.get_fields(s), fields)

        fields = {'field_1': 'value_1'}
        job = conn.register_job('test', batch_size=5)
        for s in series_ids:
            job.remove_fields(s)

        for s in series_ids:
            job.put_fields(s, fields)
            job.submit()
        job.finish()
        # now we have only new fields
        for s in series_ids:
            self.assertEqual(conn.get_fields(s), fields)

        with conn.register_job('test') as job:
            job.delete_series("sid={}".format(sid('test', 'series')), one=False, force=True)

