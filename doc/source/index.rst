.. Shooju Python Client documentation master file, created by
   sphinx-quickstart on Wed Nov 13 16:05:49 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Shooju Python Client's documentation!
================================================

Contents:

.. toctree::
   :maxdepth: 2

   quickstart.rst
   code.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

