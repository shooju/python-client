Shooju Module
===============
.. automodule:: shooju

Connection
----------
.. autoclass:: Connection
    :members:

Point
------
.. autoclass:: Point
    :members:

Shooju REST Client
--------------------

.. autoclass:: Client
    :members: