Quickstart
============

Install
-------

- Install using pip::

    pip install shooju

- From source::

    python setup.py install

Usage
-----

.. code:: python

    from shooju import Connection sid, Point
    from datetime import date

    if __name__ == '__main__':
        conn = Connection(server='https://www.shooju.com', username='my_name', api_key='abc')
        job = conn.register_job('Sample Job')

        # create series id
        series_id = sid('users', 'my_name', 'china', 'population')

        # add point to the time series
        job.put_point(series_id, Point(date(2013, 1, 1), 42)

        # add a field to the time series
        job.put_field(series_id, 'unit', 'dollars')


        # retrieve data

        point = conn.get_point(series_id, date(2013, 1, 1))

        field = conn.get_field(series_id, 'unit')


Samples
=========

Sample Configuration
---------------------

.. literalinclude:: ../../usage_samples/sample_settings.py


Basic Example
----------------

.. literalinclude:: ../../usage_samples/basic.py

Pandas Support
----------------

.. literalinclude:: ../../usage_samples/shooju_pandas.py
