from sample_settings import USERNAME, API_KEY, API_SERVER
import sys

sys.path.append("..")

from datetime import date
from shooju import Connection, Point, sid

conn = Connection(server=API_SERVER, user=USERNAME, api_key=API_KEY)

job = conn.register_job('Pandas test', batch_size=20)
series_ids = []
colors = ['blue', 'red', 'yellow', 'brown', 'pink', 'white', 'lilac', 'gray', 'green', 'black']

#Create some series
for i in range(10):
    series_id = sid('users', USERNAME, 'pandas', 'test', 's%d' % i)
    series_ids.append(series_id)

    # add points to the series
    points = [Point(date(2000 + j, 1, 1), j) for j in range(10)]
    job.put_points(series_id, points)
    job.put_field(series_id, 'color', colors[i])

job.submit()

query = r'tree:users\%s\pandas' % USERNAME

print('DataFrame only points: %s' % query)
print(conn.get_df(query, fields=[], series_axis='columns', max_points=-1))
print('\n')

print('DataFrame only points with custom column names: %s' % query)
print(conn.get_df(query, fields=['=Color: {{color}}'], series_axis='columns', max_points=-1))
print('\n')

print('DataFrame only points with custom column name (one field): %s' % query)
print(conn.get_df(query, fields=['meta.updated_by'], series_axis='columns', max_points=10))
print('\n')

print('DataFrame with Fields: %s' % query)
print(conn.get_df(query, fields=['color']))
print('\n')

print('DataFrame with Fields and Points: %s' % query)
print(conn.get_df(query, fields=['color'], max_points=-1))  # returns pandas.DataFrame
print('\n')

print('Empty DataFrame: %s' % query)
print(conn.get_df(query, fields=[], series_axis='columns'))
print('\n')
