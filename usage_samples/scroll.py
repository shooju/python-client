#initialize path and user settings
from sample_settings import USERNAME, API_KEY, API_SERVER
import sys
sys.path.append("..")
from datetime import date

from shooju import Connection, sid, Point

conn = Connection(server=API_SERVER, user=USERNAME, api_key=API_KEY)

# adding data
job = conn.register_job('Basic Test')
series_id1 = sid('users', USERNAME, 'mexico', 'population')
job.put_point(series_id1, Point(date(2013, 1, 1), 100))

job = conn.register_job('Basic Test')
series_id2 = sid('users', USERNAME, 'brazil', 'population')
job.put_point(series_id2, Point(date(2013, 1, 1), 500))

for series in conn.scroll(query=r'sid:users\{}'.format(USERNAME),
                          batch_size=100,
                          fields=['color', 'fruit'],
                          max_points=-1):
    series_id = series['series_id']
    points = series['points']
    fields = series['fields']
    print(series)