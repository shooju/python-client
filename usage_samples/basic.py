#initialize path and user settings
from sample_settings import USERNAME, API_KEY, API_SERVER
import sys
sys.path.append("..")

#----- start sample --- 

from shooju import Connection, sid, Point
from datetime import date

#connect to Shooju using username and api key on a server
conn = Connection(server=API_SERVER, user=USERNAME, api_key=API_KEY)

#register a job with no arguments
job = conn.register_job('Basic Test')

#create a series id (local representation)
series_id = sid("users", USERNAME, "china", "population")

#write a point and a field to Shooju
job.put_point(series_id, Point(date(2012, 1, 1), 314.3))
job.put_field(series_id, "unit", "millions")

#read the point and field from Shooju
print conn.get_point(series_id, date(2012, 1, 1)).value
print conn.get_field(series_id, "unit")

#register a job as context manager
with conn.register_job('Basic Test') as job:

    #create a series id (local representation)
    series_id = sid("users", USERNAME, "china", "population")

    #write a point and a field to Shooju
    job.put_point(series_id, Point(date(2013, 1, 1), 414.3))
    job.put_field(series_id, "unit", "millions")

    #read the point and field from Shooju
    print conn.get_point(series_id, date(2013, 1, 1)).value
    print conn.get_field(series_id, "unit")