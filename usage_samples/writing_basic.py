# initialize path and user settings
from sample_settings import USERNAME, API_KEY, API_SERVER
import sys

sys.path.append("..")

# ----- start sample ---

from shooju import Connection, sid, Point
from datetime import date, datetime

# connect to Shooju using username and password on a server
conn = Connection(server=API_SERVER, user=USERNAME, api_key=API_KEY)

# register a job with no arguments
with conn.register_job('Writing Basic Test') as job:

    # create a series id (local representation)
    series_id = sid("users", USERNAME, "gdp", "japan")

    # instantiate point
    pt = Point(datetime(2010, 1, 1), 8.0)

    # write the point
    job.put_point(series_id, pt)

    # write an array of points
    points = []
    for i in range(1, 28):
        points.append(Point(date(2010 + i, 1, 1), i))
    job.put_points(series_id, points)

    # write some fields
    job.put_field(series_id, 'source', 'Sam analysis')
    job.put_field(series_id, 'unit', 'US$bn')
