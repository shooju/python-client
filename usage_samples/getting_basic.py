# initialize path and user settings
from sample_settings import USERNAME, API_KEY, API_SERVER
import sys

sys.path.append("..")

# initialize data

from writing_basic import series_id

# ----- start sample ---

from shooju import Connection, sid, Point
from datetime import date

# connect to Shooju using username and password on a server
conn = Connection(server=API_SERVER, user=USERNAME, api_key=API_KEY)
series_query = 'sid="{}"'.format(series_id)

# get many points
r = conn.get_series(series_query, df=date(2011, 1, 1), dt=date(2020, 1, 1), max_points=-1)['points']
print(r[0].date, r[0].value)

# get one point
print(conn.get_series(series_query, df=date(2011, 1, 1))['points'][0].value)

# get one field value
print(conn.get_series(series_query, fields=["unit"])['fields']['unit'])

# get all the field values for a given series_id as a dictionary
print(conn.get_series(series_query, fields=['*'])['fields'])

# get some of the fields for a given series_id
print(conn.get_fields(series_query, fields=["source", "unit"])['fields'])
