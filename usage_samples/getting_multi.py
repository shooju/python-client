# initialize path and user settings
from sample_settings import USERNAME, API_KEY, API_SERVER
import sys

sys.path.append("..")

# initialize data

from writing_basic import series_id

# ----- start sample ---

from shooju import Connection, sid, Point
from datetime import date

# connect to Shooju using username and password on a server
conn = Connection(server=API_SERVER, user=USERNAME, api_key=API_KEY)
series_query = 'sid="{}"'.format(series_id)

# create a multi-get request
mget = conn.mget()

# queue five get* requests
mget.get_series(series_query, df=date(2011, 1, 1), dt=date(2020, 1, 1), max_points=-1)
mget.get_point(series_query, df=date(2011, 1, 1), max_points=1)
c = mget.get_field(series_query, fields=["unit"])
mget.get_fields(series_query, fields=['*'])
mget.get_fields(series_query, fields=["source", "unit"])

# fetch results
result = mget.fetch()

# the third result is that of get_field(series_id, "unit")
print(result[c])
