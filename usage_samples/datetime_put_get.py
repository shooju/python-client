# initialize path and user settings
from sample_settings import USERNAME, API_KEY, API_SERVER
import sys

sys.path.append("..")

# ----- start sample ---

from shooju import Connection, sid, Point
from datetime import datetime

# connect to Shooju using username and password on a server
conn = Connection(server=API_SERVER, user=USERNAME, api_key=API_KEY)

# create a series id (local representation)
series_id = sid("users", USERNAME, "intraday", "at 3pm")
series_query = 'sid="{}"'.format(series_id)

# instantiate point
pt1 = Point(datetime(2010, 1, 1, 15), 2)
pt2 = Point(datetime(2010, 1, 1), 5)

# register a job with no arguments
job = conn.register_job('Datetime Put Get Test')

# write the point
job.put_point(series_id, pt1)
job.put_point(series_id, pt2)

# get the point
pt1 = conn.get_series(series_query, df=datetime(2010, 1, 1, 15), max_points=1)['points'][0]
assert pt1.value == 2.0
assert pt1.datetime == datetime(2010, 1, 1, 15)

pt2 = conn.get_series(series_query, df=datetime(2010, 1, 1), max_points=1)['points'][0]
assert pt2.value == 5.0
assert pt2.datetime == datetime(2010, 1, 1)

# should be two points
print(conn.get_series(series_query, max_points=-1))

with conn.register_job('delete') as job:
    job.delete_series('sid={}'.format(series_id), one=True)
