# initialize path and user settings
from sample_settings import USERNAME, API_KEY, API_SERVER
import sys
import string

sys.path.append("..")

# ----- start sample ---

from shooju import Connection, sid, Point
from datetime import date

# connect to Shooju using username and password on a server
conn = Connection(server=API_SERVER, user=USERNAME, api_key=API_KEY)

# register a job with a batch size of 5
job = conn.register_job('Writing Batch Size Test', batch_size=100)

# create a series id (local representation)
series_id = sid("users", USERNAME, "gdp", "germany")

# write a lot of points
for l1 in string.uppercase:
    for l2 in string.lowercase:
        letter = l1 + l2
        series_id = sid("users", USERNAME, "batch", "germany", letter)
        points = []
        for i in range(1, 28):
            points.append(Point(date(2010 + i, 1, 1), i))
        job.put_points(series_id, points)

        # write some fields
        job.put_field(series_id, 'source', 'My analysis')
        job.put_field(series_id, 'unit', 'US$bn')

# must submit because we have made only 3 put_* calls, but the batch size is 5
job.submit()

with conn.register_job('delete') as job:
    # delete the series we just made
    job.delete_series('sid={}'.format(series_id), one=True)
